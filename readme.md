**DK-Witcher-game** - Java / Java FX game
=

- Author: Daniel Kubánek
- Supervisor: Ing.Jan Říha [linkedin](https://cz.linkedin.com/in/rihaj) (first semester - Java)
- Supervisor: Ing.Marcel Valový [linkedid](https://cz.linkedin.com/in/marcelv3612) (second semester - JavaFx)

Installation / How to play
-
At first, you need to set up local machine

- Go ``File / Project Structures`` and use same SDK like as I do.

![](src/main/resources/resources/readme-1.png "")

- If you want to run a game in **GUI**, you need to set up a configuration first. [Link to VM problem solution](https://stackoverflow.com/questions/51478675/error-javafx-runtime-components-are-missing-and-are-required-to-run-this-appli)
- My VM set up: ``--module-path "C:\Users\DK\.jdks\javafx-sdk-17.0.1\lib" --add-modules=javafx.swing,javafx.graphics,javafx.fxml,javafx.media,javafx.web --add-reads javafx.graphics=ALL-UNNAMED --add-opens javafx.controls/com.sun.javafx.charts=ALL-UNNAMED --add-opens javafx.graphics/com.sun.javafx.iio=ALL-UNNAMED --add-opens javafx.graphics/com.sun.javafx.iio.common=ALL-UNNAMED --add-opens javafx.graphics/com.sun.javafx.css=ALL-UNNAMED --add-opens javafx.base/com.sun.javafx.runtime=ALL-UNNAMED``
- This VM set up should also work: ``----module-path "C:\Users\DK\.jdks\javafx-sdk-17.0.1\lib" --add-modules javafx.controls,javafx.fxml``
- Don´t forget change a module-path to your own!

![](src/main/resources/resources/readme-2.png "")

- If you want to run the game in **command line**, it´s also needed to set up the configuration.
- My VM set up: ``--module-path "C:\Users\DK\.jdks\javafx-sdk-17.0.1\lib" --add-modules javafx.controls,javafx.fxml``
- Don´t forget change the module-path to your own!

![](src/main/resources/resources/readme-3.png "")

Introduction - CZ
-

```
Účelem projektu bylo vytvořit hru, která přiblíží život zaklínače.
Hra je inspirovaná na motivy z knih Andrzeje Sapkowského a her od vývojářů z Projekt RED. 
Mně osobně se hry a knihy strašně moc líbily, proto jsem se rozhodl hru na téma zaklínač vytvořit.
```

Game tasks / Message from author - CZ
-

```
Aby hru bylo možné vyhrát a dokončit, je potřeba splnit všechny 4 úkoly, včetně finálního.
První tři úkoly spočívají v nalezení lokací s bossy, které porazíte příkazem 'Bojuj'.
Po každém vyhraném souboji s bossem doporučuji vrátit se do vesnice a vyzvednout si finanční odměnu, za kterou si můžete postavu vylepšit nebo doplnit její životy. 
Pokud nemáte finance na vylepšení celého vybavení, doporučuji se v herním světě "poflakovat" a porážet náhodně generované protivníky. 
Po splnění všech úkolů se zpřístupní finální úkol, který je dostupný v lokaci "Hrad".

Přeji příjemnou zábavu během hraní hry.
```