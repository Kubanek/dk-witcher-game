package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz pro vylepšení hráčových předmětů.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionUpradgeItem implements IAction
{
    private Game game;

    private static final Set<String> itemsToUpradge = new HashSet<String>() {{
                add(Inventory.SWORD);
                add(Inventory.ARMOR);
                add(Inventory.POTION_SAC);
            }};
            
    private static final Map<String, Integer> upradgeCost = new HashMap<String, Integer>() {{
                put(Inventory.SWORD, Inventory.SWORD_COST);
                put(Inventory.ARMOR, Inventory.ARMOR_COST);
                put(Inventory.POTION_SAC, Inventory.POTION_SAC_COST);
            }};

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionUpradgeItem(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>jdi</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Vylepsi";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráči vylepšit předmět.";
    }

    /**
     * Metoda se pokusí vylepšit hráčův předmět. Nejprve zkontroluje lokaci.
     * Pokud se hráč nenachází u kováře, vyhodí chybovou hlášku. Dále pak zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznáme co se má vylepšít)</i>,
     * nebo bylo zadáno dva a více parametrů tj. <i>(tj. hráč chce vylepšít více 
     * věcí najednou)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda lze předmět vylepšit (meč, brnění, vaky na lekvary). Pokud ne, vyhodí chyb. 
     * hlášku. Dále zkontoluje zda má hráč finance na vylepšení, pokud ne, vyhodí chybovou hlášku.
     * Každý předmět lze vylepšit pouze jednou a pokud již byl vylepšen, vyhodí chybovou hlášku. 
     * Pokud všechny kontroly proběhnou v pořádku, odečtou se hráči peníze a vylepší se předmět
     * 
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        Area currentArea = game.getWorld().getCurrentArea();
        String currentAreaName = currentArea.getName();
        Witcher witcher = game.getWitcher();

        if (!currentAreaName.equals(GameLocation.BLACKSMITH)) {
            return "Své vybavení si můžeš vylepšit jen u kováře.";
        }

        if (parameters.length < 1) {
            return GameLocation.BLACKSMITH_UPRADGE;
        } else if (parameters.length > 1) {
            return "U kováře nelze vylepšit více předmětů najednou.";        
        }
        
        String itemName = parameters[0];
        Inventory inventory = game.getInventory();

        if (!itemsToUpradge.contains(itemName)) {
            return "Předmět '" + itemName + "' nelze u kováře vylepšit.";
        }
        
        int cost = upradgeCost.get(itemName);
        int moneyInInventory = inventory.getMoney();
        
        if (cost > moneyInInventory) {
            return "Předmět '" + itemName + "' stojí " + cost + " zlotých.\n"
            + "Ve tvém inventáři se nachází jenom " + inventory.getMoney() + " zlotých";
        }
        
        String upradgeSuccess = "Vybavení '" + itemName + "' bylo vylepšeno";
        
        if (itemName.equals(Inventory.SWORD) && !inventory.isUpradgedDamage()) {
            inventory.setMoney(cost, true);
            inventory.setIsUpradgedDamage(true);
            
            witcher.setLowerDamage(Inventory.UPRADGED_LOWER_DAMAGE);
            witcher.setUpperDamage(Inventory.UPRADGED_UPPER_DAMAGE);
            
            return upradgeSuccess;
        } else if (itemName.equals(Inventory.ARMOR) && !inventory.isUpradgedArmor()) {
            inventory.setMoney(cost, true);
            inventory.setIsUpradgedArmor(true);
            
            witcher.setDodgeNumber(Inventory.UPRADGED_DODGE_NUMBER);
            witcher.setMaxHp(Inventory.UPRADGED_MAX_HP);
            
            return upradgeSuccess;
        } else if (itemName.equals(Inventory.POTION_SAC) && !inventory.isUpradgedPotionSizeInventory()) {
            inventory.setMoney(cost, true);
            inventory.setIsUpradgedPotionSizeInventory(true);
            
            inventory.setMaximumOfHealingPotions(Inventory.UPRADGED_MAXIMUM_OF_POTIONS);
            
            return upradgeSuccess;
        }

        return GameLocation.THIS_ITEM_CANNOT_BE_UPRADGED_AGAIN;
    }
}
