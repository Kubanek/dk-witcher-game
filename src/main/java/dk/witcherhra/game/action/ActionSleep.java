package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

/**
 * Třída implementující příkaz pro 'spánek' v lokaci Hostinec.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionSleep implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionSleep(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Vyspi_se</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Vyspi_se";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráčovi doplňit všechny životy a léčebné elixíry. Možné pouze v lokaci Hostinec";
    }

    /**
     * Metoda se pokusí bojovat s. Nejprve zkontroluje, 
     * zda se hráč nachází v aktuální lokaci Hostinec. Pokud ne, vrátí
     * chybové hlášení. Poté zkontroluje počet parametrů. Pokud byly zadány 
     * nějaké parametry <i>(tj. hráč chce spát na více místech
     * současně nebo místo specifikovat)</i>, vrátí chybové hlášení.
     * Dále metoda zkonroluje, zda má hráč peníze na spánek a zda hráčova postava
     * má plné životy. Pokud nemá dostatek peněz, nebo má postava plné životy, vyhodí chybovou hlášku.
     * V moment, kdy je kontrola úspešná, hráčovi se odečtou peníze a doplní životy a léčebné lektvary.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        Area currentArea = game.getWorld().getCurrentArea();
        String currentAreaName = currentArea.getName();

        if (!currentAreaName.equals(GameLocation.PUB)) {
            return "Vyspat se můžete jen v Hostinci";
        }

        if (parameters.length >= 1) {
            return "Pokud chceš spát, není potřeba specifikovat místo";
        }

        Inventory inventory = game.getInventory();
        Witcher witcher = game.getWitcher();
        int money = inventory.getMoney();
        int cost = GameLocation.PUB_COST;

        if (cost > money) {
            return "Přespání v hospodě stojí " + cost + " zlotých.\n"
                + "Ve tvém inventáři se nachází jenom " + inventory.getMoney() + " zlotých";
        } else if (witcher.getMaxHP() == witcher.getHpBar()) {
            return "Cítíš se plný sil, nepotřebuješ spát";
        }

        inventory.setMoney(cost, true);
        witcher.setHpBar(witcher.getMaxHP());
        inventory.setNumberOfHealingPotions(inventory.getMaximumOfHealingPotions());

        return GameLocation.PUB_AFTER_SLEEPING;
    }
}
