package dk.witcherhra.game.action;

import dk.witcherhra.game.Game;
import dk.witcherhra.game.IAction;

/**
 * Třída implementující příkaz pro předčasné ukončení hry.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionTerminate implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionTerminate(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Konec</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Konec";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Ukončuje hru.";
    }

    /**
     * Metoda ukončí hru. Metoda zkontroluje počet parametrů. Pokud byly zadány 
     * nějaké parametry, vrátí chybové hlášení.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length > 0) {
            return "Pokud chcete ukončit hru, pouzijte pouze příkaz 'Konec'";
        }
        
        game.setGameOver(true);

        return "Hra byla ukončena příkazem 'Konec'.";
    }
}
