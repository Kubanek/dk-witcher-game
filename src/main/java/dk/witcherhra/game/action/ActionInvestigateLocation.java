package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz Prozkoumej_lokaci.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionInvestigateLocation implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionInvestigateLocation(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Prozkoumej_lokaci</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Prozkoumej_lokaci";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Prozkoumá blíže předměty v jednotlivé lokaci";
    }

    /**
     * Metoda vrátí detailní popis aktuální lokace. Metoda zkontroluje počet parametrů. Pokud byly zadány 
     * nějaké parametry, vrátí chybové hlášení. Dále metoda zkontroluje, zda se v lokaci nachází nějaké předměty
     * a pokud ano, vypíše se jejich popis.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length > 0) {
            return "Jako zaklínači ti stačí prozkoumat lokaci kolem sebe, nepotřebuješ se prozkoumat nic konkrétního.";
        }

        Area currentArea = game.getWorld().getCurrentArea();
        Collection<Item> items = currentArea.getItems();
        
        if (items.isEmpty()) {
            return GameLocation.IN_LOCATION_IS_NOTHING_TO_INVESTIGATE;
        }
        
        String investigationText = "V lokaci se nachází:";
        for (Item item : items) {
            investigationText += "\n" + item.getFullDescription();
        }

        return investigationText;
    }
    
}
