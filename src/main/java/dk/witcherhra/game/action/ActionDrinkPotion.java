package dk.witcherhra.game.action;

import dk.witcherhra.game.Game;
import dk.witcherhra.game.IAction;
import dk.witcherhra.game.Inventory;
import dk.witcherhra.game.Witcher;

/**
 * Třída implementující příkaz 'Vypij_elixir', který umožní hráčovi doplnit část života.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionDrinkPotion implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionDrinkPotion(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Vypij_elixir</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Vypij_elixir";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje vypít léčebný lektvar (i běhěm bitvy) a tak doplnit hráčovi životy (50 HP).";
    }

    /**
     * Metoda se pokusí vypít léčebný elixír. Nejprve zkontroluje počet
     * parametrů. Pokud byl zadán jeden a více parametrů <i>(tj. hráč chce specifikovat vypití
     * elixíru)</i>, vrátí chybové hlášení. Dále metoda zkontoluje, zda hráč vlastní elixíry. 
     * Pokud ne, vrátí chybové hlášení. Dále zkontoluje počet hráčových životů a pokud 
     * má hráč plné životy, vrátí chbové hlášení. Pokud kontroly proběhnou v pořádku, 
     * provede vypítí elixíru, přidá hráčovi životy a "odebere jeden elixír" z inventáře.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length >= 1) {
            return "Jste zaklínač, který má u sebe pouze elixíry doplnění života, nepotřebujete tedy popisovat jaky elixír chcete vypít";
        }

        Inventory inventory = game.getInventory();
        Witcher witcher = game.getWitcher();
        int currentNumberOfPotions = inventory.getNumberOfHealingPotions();
        int maximumNumberOfPotions = inventory.getMaximumOfHealingPotions();

        if (currentNumberOfPotions <= 0) {
            return "V inventáři nemátě žádný léčebný elixít na vypití.";
        } else if (witcher.getHpBar() == witcher.getMaxHP()) {
            return "Cítíš se plný sil, nepotřebuješ použít léčebný elixír.";
        }       

        witcher.setHpBarAfterHealing(Inventory.HEALING_POTION_REGENERATE);

        currentNumberOfPotions -= 1;
        inventory.setNumberOfHealingPotions(currentNumberOfPotions);
        currentNumberOfPotions = inventory.getNumberOfHealingPotions();

        return "Vypili jste léčebný lektvar. Počet aktuálních HP se změnil na " + witcher.getHpBar() + "\n"
        + "Počet dostupných lěčebných lektvarů: " + currentNumberOfPotions + " z " + maximumNumberOfPotions;
    }    
}
