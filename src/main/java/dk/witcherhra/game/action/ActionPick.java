package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

/**
 * Třída implementující příkaz pro sbírání předmětů.
 *
 * @author Daniel Kubánek
 * @author Jan Říha
 * @version LS-2021, 2021-05-10
 */
public class ActionPick implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionPick(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Seber</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Seber";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Sebere specifickou věc z lokace, pokud se tam nachází.";
    }

    /**
     * Metoda se pokusí sebrat předmět z aktuální lokace a uložit ho do hráčova
     * inventáře. Nejprve zkontroluje počet parametrů. Pokud nebyl zadán žádný
     * parametr <i>(tj. neznáme požadovaný předmět)</i>, nebo bylo zadáno dva a
     * více parametrů <i>(tj. hráč chce sebrat více předmětů současně)</i>, vrátí
     * chybové hlášení. Pokud byl zadán právě jeden parametr, zkontroluje, zda
     * v aktuální lokaci je předmět s daným názvem, zda je přenositelný a zda
     * na něj hráč má v inventáři místo <i>(tj. zda ho lze sebrat)</i>. Pokud ne,
     * vrátí chybové hlášení. Pokud všechny kontroly proběhnou v pořádku, provede
     * přesun předmětu z lokace do inventáře a vrátí informaci o sebrání předmětu.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            return "Jste zaklínač, ne čaroděj => pokud chcete něco sebrat, zadejte předmět";
        }

        if (parameters.length > 1) {
            return "Pokud chcete vzít všechny věci v lokaci, zadejte příkaz 'Seber_vse'";
        } 

        String itemName = parameters[0];
        Area currentArea = game.getWorld().getCurrentArea();

        if (!currentArea.containsItem(itemName)) {
            return "Předmět '" + itemName + "' tady není.";
        }

        Item item = currentArea.getItem(itemName);
        Inventory inventory = game.getInventory();

        if (!item.isMoveable()) {
            return "Jste zaklínač, předmět '" + itemName + "' fakt nepotřebuješ.";
        } else if (inventory.isInventoryFull()) {
            return Inventory.INVENTORY_IS_FULL;
        }

        inventory.addItemToInventory(item);
        currentArea.removeItem(itemName);

        return "Sebral jsi předmět '" + itemName + "' a uložil jsi ho do inventáře.";
    }
}
