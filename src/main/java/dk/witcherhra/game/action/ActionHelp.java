package dk.witcherhra.game.action;

import dk.witcherhra.game.Game;
import dk.witcherhra.game.IAction;

import java.util.*;

/**
 * Třída implementující příkaz pro zobrazení nápovědy ke hře.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-23
 */
public class ActionHelp implements IAction
{
    private Game game;
    
    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionHelp(Game game)
    {
        this.game = game;
    }
    
    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Napoveda</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Napoveda";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Vypisuje všechny dostupné herní příkazy.";
    }

    /**
     * Metoda vrací obecnou nápovědu ke hře. Nyní pouze vypisuje vcelku
     * primitivní zprávu o herním příběhu.
     *
     * @param parameters parametry příkazu <i>(aktuálně se ignorují)</i>
     * @return nápověda, kterou hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length >= 1) {
            return "Pro zobrazení nápovědy, stačí pouze zadat příkaz 'Napoveda'.";
        }

        HashSet<IAction> actions = game.getActions();
        String helpText = "";
        
        for (IAction action : actions) {
            helpText += action.getName() + " - " + action.getDesc() + "\n";
        }
        
        return helpText;
    }
}
