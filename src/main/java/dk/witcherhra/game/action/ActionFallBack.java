package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

/**
 * Třída implementující příkaz pro únik do předchozí lokace.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionFallBack implements IAction
{

    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionFallBack(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Vrat_se</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Vrat_se";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Vrátí hráče do předchozí lokace (i během bitvy) (ve finálové lokaci není možný návrat).";
    }
    
    /**
     * Metoda se pokusí přesunout hráče do předchozí lokace. Nejprve zkontroluje počet
     * parametrů. Pokud bylo zadáno jeden a více parametrů, vrátí chybové hlášení. 
     * Pokud nebyl zadán parametr hráč je přesunut do přechozí lokace. Pokud se hráč vrátí z předchozí lokace
     * a bude chtít pak znova útect do přechozí lokace, vrátí chybové hlášení.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length >= 1) {
            return "Pokud se chcete přesunout do předchozí lokace, není potřeba lokaci vypisovat";
        }

        GameWorld gameWorld = game.getWorld();

        Area currentArea = gameWorld.getCurrentArea();
        int currentAreaId = currentArea.getId();

        Area previousArea = gameWorld.getPreviousArea();
        int previousAreaId = previousArea.getId();

        if (currentAreaId == previousAreaId) {
            return GameLocation.FAILED_PREVIOUS_LOCATION_CHANGE;
        } else if (currentArea.getName().equals(GameLocation.DRAGON_CAVE)) {
            return "Z této lokace není cesta zpět";
        }
        
        gameWorld.setCurrentArea(previousArea);

        currentArea = gameWorld.getCurrentArea();
        Witcher witcher = game.getWitcher();
        Enemy enemy;

        if(currentArea.canSpawnEnemy()) {
            if (currentArea.hasEnemy()) {
                enemy = currentArea.getEnemy();

                return currentArea.getFullDescription() + "\n---\n" + "Před sebou vidíš nebojácně stát protivníka.\n\n" + enemy.getDescription() + "\n---\n" + witcher.getDescription();
            } else if (currentArea.isSpawnedNewEnemy()) {
                enemy = Encounters.getRandomEnemy();
                currentArea.setEnemy(enemy);
                gameWorld.setCurrentArea(currentArea);

                return currentArea.getFullDescription() + "\n---\n" + "Před sebou vidíš nebojácně stát protivníka.\n\n" + enemy.getDescription() + "\n---\n" + witcher.getDescription();
            }
        }

        return currentArea.getFullDescription();
    }
}
