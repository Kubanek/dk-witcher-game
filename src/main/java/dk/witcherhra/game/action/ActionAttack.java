package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

/**
 * Třída implementující příkaz pro boj.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionAttack implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionAttack(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Bojuj</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Bojuj";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráči boj s nepříteli.";
    }
    
    /**
     * Metoda se pokusí bojovat s nepřítelem. Nejprve zkontroluje počet
     * parametrů. Pokud byly zadány nějaké parametry <i>(tj. hráč chce bojovat s více
     * nepříteli současně nebo nepřítele specifikovat)</i>, vrátí chybové hlášení. 
     * Poté metoda zkontoluje, zda je v lokaci nepřítel a pokud ne, vyhodí chybovou 
     * hlášku. Pokud kontrola proběhne v pořádku, metoda pomocí randomizace čísel
     * vyhodnotí příkaz a udělí hráči/nepříteli poškození, popřípadě neudělí žádné
     * poškození, pokud se hráč/nepřítel útoku vyhnul.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length >= 1) {
            return "Jste zaklínač, nepotřebujete specificky popisovat na koho útočíte, vždycky to jako zaklínač víte.";
        }

        Area currentArea = game.getWorld().getCurrentArea();

        if (!currentArea.hasEnemy()) {
            return GameLocation.NOBODY_TO_FIGHT;
        }

        Inventory inventory = game.getInventory();
        Enemy enemy = currentArea.getEnemy();
        Witcher witcher = game.getWitcher();

        int witcherDmg = witcher.damageDealed();
        int enemyDmg = enemy.damageDealed();

        int witcherCurrentHp = witcher.getHpBar();
        int enemyCurrentHp = enemy.getHpBar();

        String enemyName = enemy.getName();

        String attackResult = "";
        int reward = 0;

        if (!enemy.isAttackDodged()) {
            enemyCurrentHp -= witcherDmg;
            enemy.setHpBar(enemyCurrentHp);
            enemyCurrentHp = enemy.getHpBar();

            attackResult += "Zaklínač svou ranou údeřil '" + enemyName + "' za " + witcherDmg + " dmg.\n";

            if (enemy.isDead()) {
                currentArea.setEnemy(null);

                if (enemy.isBoss()) {
                    if (enemyName == Bosses.DRAGON_BOSS) {
                        game.setGameOver(true);
                        
                        return GameConversation.AFTER_FINAL_BATTLE;
                    }
                    
                    if (enemyName == Bosses.BANDIT_BOSS) {
                        game.getTasks().setTaskOneComplete(true);
                    } else if (enemyName == Bosses.TROLL_BOSS) {
                        game.getTasks().setTaskTwoComplete(true);
                    } else if (enemyName == Bosses.KIDNAPPER_BOSS) {
                        game.getTasks().setTaskThreeComplete(true);
                    }

                    reward = enemy.getRewardAfterKillBoss();
                    inventory.setMoney(reward);
                    witcher.setHpBarAfterHealing(Inventory.AFTER_BOSS_REGENERATE);

                    return attackResult += "'" +  enemyName + "' umřel. U nepřítele jsi našel " + reward + " zlotých.\n\n" + GameConversation.AFRER_BOSS_FIGHT + "\n---\n" + currentArea.getFullDescription();
                }

                reward = enemy.getRewardAfterKillEnemy();
                inventory.setMoney(reward);

                return attackResult += "'" +  enemyName + "' umřel. U nepřítele jsi našel " + reward + " zlotých.\n---\n" + currentArea.getFullDescription();
            } else {
                attackResult += "'" +  enemyName + "' zbývá " + enemyCurrentHp + " HP.\n---\n";
            }
        } else {
            attackResult += "'" + enemyName + "' se vyhýbá.\n---\n";
        }

        if (!witcher.isAttackDodged()) {
            witcher.setHpBar(witcherCurrentHp - enemyDmg);
            witcherCurrentHp = witcher.getHpBar();

            attackResult += "'" + enemyName+ "' zasahuje zaklínače za " + enemyDmg + " dmg.\n";
            if (witcher.isDead()) {
                game.setGameOver(true);
                witcher.setIsDead();

                return attackResult + "\nZaklínač zemřel.";
            } else {
                attackResult += "Zaklínači zbývá " + witcherCurrentHp + " HP.";
            }
        } else {
            attackResult += "Zaklínač se obratně úderu vyhýbá.";
        }

        return attackResult;
    }
}
