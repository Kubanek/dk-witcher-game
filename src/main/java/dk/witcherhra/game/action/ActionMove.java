package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz pro pohyb mezi herními lokacemi.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionMove implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionMove(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>jdi</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Jdi";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje přesun mezi lokaci";
    }

    /**
     * Metoda se pokusí přesunout hráče do jiné lokace. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznáme cíl cesty)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. hráč chce jít na více míst
     * současně)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda s aktuální lokací sousedí lokace s daným názvem <i>(tj.
     * zda z aktuální lokace lze jít do požadovaného cíle)</i>. Pokud ne, vrátí
     * chybové hlášení. Přesun do jinačí lokace v moment, kdy se nachází v lokaci
     * nepřítel, vyhodí chybovou hlášku. Dále Metoda implementuje další herní možnosti.
     * Pokud všechny kontroly proběhnou v pořádku, provede přesun hráče do cílové lokace 
     * a vrátí její popis.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            return "K přesunu je potřeba říct, kam se má zaklínač přemístit.";
        }

        if (parameters.length > 1) {
            return "Jste bohužel jenom zaklínač, neumíte jít do více lokací současně najednou.";
        }

        String areaName = parameters[0];
        Area currentArea = game.getWorld().getCurrentArea();
        Tasks tasks = game.getTasks();

        if (!tasks.areTaskAssigned()) {
            game.getTasks().setTaskAssigned(true);
            Area targetArea = currentArea.getExit(areaName);
            game.getWorld().setCurrentArea(targetArea);

            return GameConversation.FIRST_PUB_VISIT;
        }

        if (currentArea.hasEnemy()) { //zamezuje pohyb
            return GameLocation.FAILED_CURRENT_LOCATION_CHANGE;
        }

        if (!currentArea.hasExit(areaName)) {
            return "Do lokace '" + areaName + "' se odsud jít nedá.";
        }

        if (areaName.equals(GameLocation.CASTLE) && !tasks.areTasksFinished()) {
            game.getWorld().setPreviousArea(currentArea);

            return GameConversation.CASTLE_VISIT_BEFORE_TAKS_FINISH 
            + currentArea.getFullDescription();
        }

        Area targetArea = currentArea.getExit(areaName);
        game.getWorld().setPreviousArea(currentArea);
        game.getWorld().setCurrentArea(targetArea);

        if (tasks.areTasksFinished() && !tasks.getFirstCastleVisit() && areaName.equals(GameLocation.CASTLE)) {
            tasks.setFirstCastleVisit(true);

            return GameConversation.FIRST_CASTLE_VISIT;
        }

        currentArea = game.getWorld().getCurrentArea();
        Witcher witcher = game.getWitcher();
        Enemy enemy;
        if(currentArea.canSpawnEnemy()) {
            if (currentArea.hasEnemy()) {
                enemy = currentArea.getEnemy();

                return currentArea.getFullDescription() + "\n---\n" + "Před sebou vidíš nebojácně stát protivníka.\n\n" + enemy.getDescription() + "\n---\n" + witcher.getDescription();
            } else if (currentArea.isSpawnedNewEnemy()) {
                enemy = Encounters.getRandomEnemy();
                currentArea.setEnemy(enemy);
                game.getWorld().setCurrentArea(currentArea); //added because of not correctly working observer

                return currentArea.getFullDescription() + "\n---\n" + "Před sebou vidíš nebojácně stát protivníka.\n\n" + enemy.getDescription() + "\n---\n" + witcher.getDescription();
            }
        } else if (currentArea.hasEnemy()) {
            enemy = currentArea.getEnemy();

            return currentArea.getFullDescription() + "\n---\n" + "Před sebou vidíš nebojácně stát protivníka.\n\n" + enemy.getDescription() + "\n---\n" + witcher.getDescription();
        }

        String currentAreaName = currentArea.getName();
        Inventory inventory = game.getInventory();
        int reward = Tasks.TASK_REWARD;
        String taskCompleteText = "";

        if (currentAreaName.equals(GameLocation.VILLAGE)) {
            if (tasks.isTaskOneComplete() && !tasks.isAwardedTaskOne()) {
                inventory.setMoney(reward);
                tasks.setAwardedTaskOne(true);

                taskCompleteText += GameConversation.TASK_ONE_FINISHED + "\n";
            }

            if (tasks.isTaskTwoComplete() && !tasks.isAwardedTaskTwo()) {
                inventory.setMoney(reward);
                tasks.setAwardedTaskTwo(true);

                taskCompleteText += GameConversation.TASK_TWO_FINISHED + "\n";
            }

            if (tasks.isTaskThreeComplete() && !tasks.isAwardedTaskThree()) {
                inventory.setMoney(reward);
                tasks.setAwardedTaskThree(true);

                taskCompleteText += GameConversation.TASK_THREE_FINISHED + "\n";
            }

            if (!taskCompleteText.isEmpty()) {
                if (tasks.isTaskOneComplete() && tasks.isTaskTwoComplete() && tasks.isTaskThreeComplete()) {
                    tasks.setTasksFinished(true);

                    taskCompleteText += "---\n" + GameConversation.TASK_FINISHED;
                }

                return taskCompleteText + "\nU sebe v Inventáři máš " + inventory.getMoney() + " zlotých.";           
            }
        }

        if (areaName.equals(GameLocation.DRAGON_CAVE)) {
            game.getWorld().setPreviousArea(game.getWorld().getCurrentArea());
        }

        return currentArea.getFullDescription();
    }
}
