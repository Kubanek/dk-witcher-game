package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz pro sbírání všech předmětů v lokaci.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionPickAll implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionPickAll(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>seber_vse</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Seber_vse";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráčovi vzít všechny předměty v lokaci";
    }

    /**
     * Metoda se pokusí sebrat všechny předměty z aktuální lokace a uložit je do hráčova
     * inventáře. Pokud byly zadány nějaké parametry <i>(tj. hráč chce specifikovat co vse
     * má postava sebrat)</i>, vrátí chybové hlášení. Pokud nebyl zadán žádný parametr,
     * metoda zkontroluje, zda se v lokaci nachází nějaké předměty a pokud ne, vrátí
     * chybové hlášení. Poté metoda sebere všechny předměty z lokace, které jdou sebrat a 
     * uloží je do invetnáře.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length >= 1) {
            return "Pro sebrání všech věcí z dané lokace není potřeba vypisovat jednotlivé předměty";
        }

        Area currentArea = game.getWorld().getCurrentArea();

        Collection<Item> items = currentArea.getItems();
        if (items.isEmpty()) {
            return "V této místnosti žádné předměty nejsou.";
        }

        String itemNames = "Sebral(a) jste tyto předměty:";
        Inventory inventory = game.getInventory();

        for (Item item : items) {
            String itemName = item.getName();
            if (!inventory.isInventoryFull()) {
                if (item.isMoveable()) {
                    itemNames += " " + itemName + ",";

                    inventory.addItemToInventory(item);
                    currentArea.removeItem(itemName);

                    game.getWorld().setCurrentArea(currentArea);
                }

                continue;
            }

            return Inventory.INVENTORY_IS_FULL + " " + itemNames;
        }

        return itemNames;
    }

}
