package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

/**
 * Třída implementující příkaz pro 'odpočinutí' v nevěstinci.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionHaveFun implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionHaveFun(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Odpocin_si</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Odpocin_si";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráčovi doplňit část životů. Možné pouze v lokaci Nevestinec";
    }
    
    /**
     * Metoda se pokusí doplnit hráčovi část životů. Nejprve zkontroluje, 
     * zda se hráč nachází v aktuální lokaci Nevěstinec. Pokud ne, vrátí
     * chybové hlášení. Poté zkontroluje počet parametrů. Pokud byly zadány 
     * nějaké parametry <i>(tj. hráč chce odpočívat na více místech
     * současně nebo místo specifikovat)</i>, vrátí chybové hlášení.
     * Dále metoda zkonroluje, zda má hráč peníze na odpocinutí a zda hráčova postava
     * má plné životy. Pokud nemá dostatek peněz, nebo má postava plné životy, vyhodí chybovou hlášku.
     * V moment, kdy je kontrola úspešná, hráčovi se odečtou peníze a přičtou životy.
     *
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        Area currentArea = game.getWorld().getCurrentArea();
        String currentAreaName = currentArea.getName();

        if (!currentAreaName.equals(GameLocation.BROTHEL)) {
            return "Odpočinout si (užít si) můžete jen v nevěstinci";
        }

        if (parameters.length >= 1) {
            return "Pokud si chceš odpočinout (užít), není potřeba specifikovat místo";
        }

        Inventory inventory = game.getInventory();
        Witcher witcher = game.getWitcher();
        int money = inventory.getMoney();
        int cost = GameLocation.BROTHEL_COST;

        if (cost > money) {
            return "Navštívení nevěstince stojí " + cost + " zlotých.\n"
            + "Ve tvém inventáři se nachází jenom " + inventory.getMoney() + " zlotých";
        } else if (witcher.getMaxHP() == witcher.getHpBar()) {
            return "Cítíš se plný sil, nepotřebuješ si odpočinout (užít).";
        }

        inventory.setMoney(cost, true);
        witcher.setHpBarAfterHealing(GameLocation.BROTHEL_HEALING);

        return GameLocation.BROTHEL_AFTER_HAVING_FUN;
    }
}
