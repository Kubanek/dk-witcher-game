package dk.witcherhra.game.action;

import dk.witcherhra.game.Area;
import dk.witcherhra.game.Game;
import dk.witcherhra.game.IAction;

/**
 * Třída implementující příkaz 'rozhlednuti se' po lokaci.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionLookAround implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionLookAround(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Rozhledni_se</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Rozhledni_se";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráči se rozhlédnout se po lokaci";
    }

    /**
     * Metoda vrátí detailní popis aktuální lokace. Metoda zkontroluje počet parametrů. Pokud byly zadány 
     * nějaké parametry, vrátí chybové hlášení.
     *
     * @param parameters parametry příkazu <i>(očekává se prázdné pole)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length > 0) {
            return "Jako zaklínači Ti stačí se porozhlednout kolem sebe, nepotřebeš se dívat na nic konkrétního.";
        }

        Area currentArea = game.getWorld().getCurrentArea();

        return currentArea.getFullDescription();
    }
}
