package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz 'Vyhod_vse', který "Odstraní" všechny předměty z hráčovo inventáře do aktuální lokace.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionDropAll implements IAction
{ 

    private Game game;

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Vyhod_vse</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Vyhod_vse";
    }

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionDropAll(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje hráči vyhodit všechny předměty z inventáře. Předměty jsou přemístěny do aktuální lokace.";
    }

    /**
     * Metoda se pokusí vyhodit všechny hráčovi předměty z inventáře. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl bylo zadáno jeden a více parametrů tj. <i>(tj. hráč chce vyhodit 
     * specifické předměty najednou)</i>, vrátí chybové hlášení. Pokud je inventář prázdný, vyhodí
     * chybovou hlášku. Pokud všechny kontroly proběhnou v pořádku, "odstraní" všechny předměty 
     * z inventáře a přesune předměty do aktuální lokace
     * 
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {

        if (parameters.length >= 1) {
            return "Pokud chcete vyhodit všechny předměty z inventáře, není potřeba vypisovat jednotlivé předměty";
        }

        Area currentArea = game.getWorld().getCurrentArea();
        Inventory inventory = game.getInventory();
        Collection<Item> inventoryItems = inventory.getItemsFromInventory();

        if (inventoryItems.isEmpty()) {
            return Inventory.INVENTORY_IS_EMPTY;
        }

        for (Item inventoryItem : inventoryItems) {
            String itemName = inventoryItem.getName();

            currentArea.addItem(inventoryItem);
            inventory.removeItemFromInventory(itemName);
        }

        return Inventory.ALL_ITEMS_WERE_REMOVE_FROM_INVENTORY;
    }

}
