package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz 'postava', který shrnuje všechny pro hráče "důležité" informace (inventář).
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionCharacter implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionCharacter(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Postava</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Postava";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožnuje hráči zjistit důležité infomace spojené s postavou (poškození, peníze, inventář atd.).";
    }
    
    /**
     * Metoda se pokusí vypsat pro hráče důležité informace předmět z inventáře. Nejprve zkontroluje počet
     * parametrů. Pokud bylo zadáno jeden a více parametrů, vrátí chybové hlášení.
     * Pokud všechny kontroly proběhnou v pořádku, propíše metoda hráči do konzole
     * všechny důležité herní údaje (poškození, počet životů, finance atd.).
     * 
     * @param parameters parametry příkazu <i>(očekává se pole bez prvku)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length > 0) {
            return "Jsi zaklínač, a víš co vlastníš. Nepotřebuješ hledat nic konkrétního.";
        }

        Inventory inventory = game.getInventory();
        Collection<Item> inventoryItems = inventory.getItemsFromInventory();
        Witcher witcher = game.getWitcher();

        String text = witcher.getDescription();
        text += "\n---\n" + inventory.getTextedMoney()
            + "\n" + inventory.getTextedCurrentNumberOfPotions();

        text += "\n---\nVe tvém inventáři se nachází:";
        for (Item inventoryItem : inventoryItems) {
            String itemName = inventoryItem.getName();
            String itemDesc = inventoryItem.getDescription();
            int itemHealingNumber = inventoryItem.getHealingNumber();

            text += "\n" + itemName + "[HP: " + itemHealingNumber + "]" + " - " + itemDesc;
        }

        return text;
    }

}
