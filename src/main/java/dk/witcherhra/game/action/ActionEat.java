package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz pro snězení předmětu v inventáři.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class ActionEat implements IAction
{
    private Game game;

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionEat(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Snez</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Snez";
    }
    
    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožňuje sníst předmět z inventáře (i běhěm bitvy) a tak doplnit hráči životy";
    }

    /**
     * Metoda se pokusí sníst předmět co se nachází v hráčově inventáři. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznáme co se má sníst)</i>,
     * nebo bylo zadáno dva a více parametrů <i>(tj. hráč chce sníst více předmětů
     * současně)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda se předmět nachází v inventáři a pokud není, vrátí chybovou hlášku. 
     * Pokud má hráč plný život, vrátí také chybovou hlášku.
     * Pokud všechny kontroly proběhnou v pořádku, provede snězení předmětu a tedy 
     * obnoví část života.
     *
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            return "Pokud chcete něco sníst, je třeba specifikovat co to má být";
        } else if (parameters.length > 1) {
            return "Jste jenom zaklínač, neumíte sníst všechno najednou.";
        }

        String itemName = parameters[0];
        Inventory inventory = game.getInventory();
        Witcher witcher = game.getWitcher();
        Collection<Item> inventoryItems = inventory.getItemsFromInventory();

        if (inventoryItems.isEmpty()) {
            return Inventory.INVENTORY_IS_EMPTY;
        } else if (!inventory.containsItem(itemName)) {
            return "V inventáři žádný předmět '" + itemName + "' není.";
        } else if (witcher.getMaxHP() == witcher.getHpBar()) {
            return "Cítíš se plný sil, nepotřebuješ nic jíst.";
        }

        Item item = inventory.getItem(itemName);
        
        witcher.setHpBarAfterHealing(item.getHealingNumber());
        inventory.removeItemFromInventory(itemName);

        return "Snědl jste předmět '" + itemName + "' Počet aktuálních HP se změnil na " + witcher.getHpBar() + ".";
    }
}
