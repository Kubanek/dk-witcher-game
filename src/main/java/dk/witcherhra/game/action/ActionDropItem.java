package dk.witcherhra.game.action;

import dk.witcherhra.game.*;

import java.util.*;

/**
 * Třída implementující příkaz pro zahození hráčových předmětů.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-11
 */
public class ActionDropItem implements IAction
{

    private Game game;

    /**
     * Metoda vrací název příkazu tj.&nbsp; slovo <b>Vyhod</b>.
     *
     * @return název příkazu
     */
    @Override
    public String getName()
    {
        return "Vyhod";
    }

    /**
     * Metoda vrací popis příkazu</b>.
     *
     * @return popis příkazu
     */
    @Override
    public String getDesc()
    {
        return "Umožnuje hráči vyhodit specifický předmět. Předmět je přemístěn do aktuální lokace.";
    }

    /**
     * Konstruktor třídy.
     *
     * @param game hra, ve které se bude příkaz používat
     */
    public ActionDropItem(Game game)
    {
        this.game = game;
    }

    /**
     * Metoda se pokusí vyhodit hráčův předmět z inventáře. Nejprve zkontroluje počet
     * parametrů. Pokud nebyl zadán žádný parametr <i>(tj. neznáme předmět co se má vyhodit)</i>,
     * nebo bylo zadáno dva a více parametrů tj. <i>(tj. hráč chce vyhodit více 
     * předmětů najednou)</i>, vrátí chybové hlášení. Pokud byl zadán právě jeden parametr,
     * zkontroluje, zda je inventář prázdný (pokud ano vyhodí chyb. hlášku)
     * a zda se předmět nachází v inentáři(pokud ne vyhodí chyb. hlášku).
     * Pokud všechny kontroly proběhnou v pořádku, "odstraní" se předmět z inventáře
     * a přesune se do aktuální lokace
     * 
     * @param parameters parametry příkazu <i>(očekává se pole s jedním prvkem)</i>
     * @return informace pro hráče, které hra vypíše na konzoli
     */
    @Override
    public String execute(String[] parameters)
    {
        if (parameters.length < 1) {
            return "Pokud chcete nějaký přemět vyhodit z inventáře, je potřeba říct, jaký předmět to má být";
        }

        if (parameters.length > 1) {
            return "Pokud chcete vyhodit z inventáře všechny předmety, zadejte příkaz Vyhod_vse";
        }

        String itemName = parameters[0];
        Inventory inventory = game.getInventory();
        Collection<Item> inventoryItems = inventory.getItemsFromInventory();

        if (inventoryItems.isEmpty()) {
            return Inventory.INVENTORY_IS_EMPTY;
        } else if (!inventory.containsItem(itemName)) {
            return "V inventáři žádný předmět '" + itemName + "' není.";
        }

        Item item = inventory.getItem(itemName);
        Area currentArea = game.getWorld().getCurrentArea();

        currentArea.addItem(item);
        inventory.removeItemFromInventory(itemName);

        return "Zahodil jsi předmět '" + itemName + "'. Tenhle předmět se teď nachází v této lokaci." ;
    }

}
