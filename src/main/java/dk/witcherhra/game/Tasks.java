package dk.witcherhra.game;

/**
 * Třída spravujeherní úkoly, jejich plnění a odměny.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 */
public class Tasks
{
    public static int TASK_REWARD = 100;
    
    private boolean areTaskAssigned; //Jsou úkoly zadány
    private boolean taskOneComplete; //Úkol s bandity
    private boolean taskTwoComplete; //Úkol s trolem
    private boolean taskThreeComplete; //Úkol se starostovou dcerou
    private boolean areTasksFinished;
    private boolean firstCastleVisit;
    private boolean awardedTaskOne; //Je získána odměna za úkol 1
    private boolean awardedTaskTwo; //Je získána odměna za úkol 2
    private boolean awardedTaskThree; //Je získána odměna za úkol 3
    
    /**
     * Metoda vrací jestli jsou úkoly zadány.
     *
     * @return {@code true}, pokud jsou úkoly zadány; jinak {@code false}
     */
    public boolean areTaskAssigned()
    {
        return areTaskAssigned;
    }

   /**
     * Metoda nastavuje, jestli byly úkoly přiděleny.
     *
     * @param taskAssigned byly úkoly přiděleny
     */
    public void setTaskAssigned(boolean taskAssigned)
    {
        this.areTaskAssigned = taskAssigned;
    }
    
    /**
     * Metoda vrací jestli jsou úkoly dokončeny.
     *
     * @return {@code true}, pokud jsou úkoly dokončeny; jinak {@code false}
     */
    public boolean areTasksFinished()
    {
        return areTasksFinished;
    }

    /**
     * Metoda nastavuje, jestli byl navštíven hrad.
     *
     * @param firstCastleVisit byl hrad navštíven
     */
    public void setFirstCastleVisit(boolean firstCastleVisit)
    {
        this.firstCastleVisit = firstCastleVisit;
    }
    
    /**
     * Metoda vrací jestli hráč navštívil hrad.
     *
     * @return {@code true}, pokud byl hráč navštívil hrad; jinak {@code false}
     */
    public boolean getFirstCastleVisit()
    {
        return firstCastleVisit;
    }
    
    /**
     * Metoda nastavuje, jestli byly úkoly splněny.
     *
     * @param taskFinished byly úkoly dokončeny
     */
    public void setTasksFinished(boolean taskFinished)
    {
        this.areTasksFinished = taskFinished;
    }
    
    /**
     * Metoda nastavuje, jestli byl první úkol splněn.
     *
     * @param isComplete byl úkol 1 splněn
     */
    public void setTaskOneComplete(boolean isComplete)
    {
        this.taskOneComplete = isComplete;
    }
    
    /**
     * Metoda vrací jestli je první úkol dokončen.
     *
     * @return {@code true}, pokud je první úkol dokončen; jinak {@code false}
     */
    public boolean isTaskOneComplete()
    {
        return taskOneComplete;
    }
    
    /**
     * Metoda vrací jestli byl hráč odměněl za první úkol.
     *
     * @return {@code true}, pokud byl hráč odměněn za první úkol; jinak {@code false}
     */
    public boolean isAwardedTaskOne()
    {
        return awardedTaskOne;
    }
    
    /**
     * Metoda nastavuje, jestli byla dána odměna za první úkol.
     *
     * @param awardedTaskOne byla dána odměna za první úkol
     */
    public void setAwardedTaskOne(boolean awardedTaskOne)
    {
        this.awardedTaskOne = awardedTaskOne;
    }
    
    /**
     * Metoda nastavuje, jestli byl druhý úkol splněn.
     *
     * @param isComplete byl úkol 2 splněn
     */
    public void setTaskTwoComplete(boolean isComplete)
    {
        this.taskTwoComplete = isComplete;
    }
    
    /**
     * Metoda vrací jestli je druhý úkol dokončen.
     *
     * @return {@code true}, pokud je druhý úkol dokončen; jinak {@code false}
     */
    public boolean isTaskTwoComplete()
    {
        return taskTwoComplete;
    }

    /**
     * Metoda vrací jestli byl hráč odměněl za druhý úkol.
     *
     * @return {@code true}, pokud byl hráč odměněn za druhý úkol; jinak {@code false}
     */
    public boolean isAwardedTaskTwo()
    {
        return awardedTaskTwo;
    }
    
    /**
     * Metoda nastavuje, jestli byla dána odměna za druhý úkol.
     *
     * @param awardedTaskTwo byla dána odměna za druhý úkol
     */
    public void setAwardedTaskTwo(boolean awardedTaskTwo)
    {
        this.awardedTaskTwo = awardedTaskTwo;
    }
    
    /**
     * Metoda nastavuje, jestli byl třetí úkol splněn.
     *
     * @param isComplete byl úkol 3 splněn
     */
    public void setTaskThreeComplete(boolean isComplete)
    {
        this.taskThreeComplete = isComplete;
    }
    
    /**
     * Metoda vrací jestli je třetí úkol dokončen.
     *
     * @return {@code true}, pokud je třetí úkol dokončen; jinak {@code false}
     */
    public boolean isTaskThreeComplete()
    {
        return taskThreeComplete;
    }
    
    /**
     * Metoda vrací jestli byl hráč odměněl za třetí úkol.
     *
     * @return {@code true}, pokud byl hráč odměněn za třetí úkol; jinak {@code false}
     */
    public boolean isAwardedTaskThree()
    {
        return awardedTaskThree;
    }
    
    /**
     * Metoda nastavuje, jestli byla dána odměna za třetí úkol.
     *
     * @param awardedTaskThree byla dána odměna za třetí úkol
     */
    public void setAwardedTaskThree(boolean awardedTaskThree)
    {
        this.awardedTaskThree = awardedTaskThree;
    }
    
}
