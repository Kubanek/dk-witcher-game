package dk.witcherhra.game;

/**
 * Třída obsahuje statické herní texty.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 */
public class GameConversation
{
    public static String FIRST_PUB_VISIT = "Zaklínač vešel do hospody. Ihned vzbudil pozornost všech, žádný válečník nenosí svůj meč na zádech jako luk a žádný válečník nemá pronikavé žluté oči.\n" 
        + "'Co tu chceš cizáku, nikdo tě tu nechce.' ozval se jeden z ochlastů.\n" 
        + "'Pivo' odpověděl Geralt klidně, 'a taky hledám práci.'\n"
        + "'Jo tak práci bys chtěl? Táhni odsaď, než ti ten tvůj meč zarazím dovnitř tvého těla.'\n"
        + "S Geraltem tato slova ani nehnula, byl už zvyklý, že pro zaklínače na tomto světě není ani místa. 'Dám si tedy aspoň to pivo, když tu práci zaklínače nepotřebujete.'\n"
        + "'Táhni odsaď, nebo...'\n"
        + "V tu chvíli přišel do hospody starosta, zpráva o zaklínačí se po vesnici roznesla velice rychle.\n"
        + "'Pane zaklínači, dobře že jste tady... Doufám, že vás slova těchto pobudů neurazila, hledáte práci?'\n"
        + "'Ano, hledám' odpověděl zaklínač.\n"
        + "'V tom případě vítejte pane zaklínači, mám tu pro vás tyto úkoly:' \n1)Zbav nás banditů \n2)Vypořádej se s agresivním trolem \n3)Najdi mou milovanou dceru\n"
        + "'Samozřejmě tě za každou prácí náležitě odměním (100 zlotých/úkol).'\n"
        + "'Kdo se opováží jenom špatným pohledem na pana zaklínače podívat, toho osobně nechám zlynčovat. Chovejte se k němu jako k místnímu.'\n"
        + "---\nAbys získal přehled o lokaci, doporučuji použít příkaz 'Rozhledni_se' a 'Prozkoumej_lokaci'.";

    public static String AFRER_BOSS_FIGHT = "Po náročném souboji ses rozhodl nachvilku odpočinout (automaticky se ti přidalo 20 HP).\n" 
        + "Zbavil si vesnice jednoho z problému, nemůžeš se už dočkat své odměny.";

    public static String TASK_ONE_FINISHED = "Již zdálky sis všiml, že tvůj příchod do vesnice očekává starosta s měšcem plných peněz.\n" 
        + "'Vidím, že zvěsti o zničení tábora se šíří rychleji, než bych čekal' řekl Zaklínač překvapeně.\n"
        + "'To víš zaklínači, tihle banditi nám kradli naší úrodu už hodně dlouho a pozabíjeli hodně našich' řekl smutně starosta.\n"
        + "'Bude chvilku trvat, než banditi budou schopni své útoky provádět s takovou efektivností jako doteď' opáčil zaklínač.\n"
        + "'S tímhle jsem bohužel počítal' povzdzchl si starosta.\n"
        + "'Do doby než banditi zvolí nového vůdce, bude stačit, když vaše vozky a zemědělce bude chránit městká militaria. To by je mělo odradit.'\n"
        + "'Budu ti muset asi věřit zaklínači. Snad nám dlouhodobě král pro naše obyvatelstvo zajistí ochranu. Na tady máš svou odměnu.' (získal jsi 100 zlotých)\n";

    public static String TASK_TWO_FINISHED = "'Bože zaklínači, ty ale vypadáš.' Rychle, zavolejte někdo lékaře!\n" 
        + "'Není třeba pane starosto, i když si vaši laskavosti cením. Jsem zaklínač, tohle je moje práce a taky, kdo by chtěl ošětřovat zaklínače.'\n"
        + "'Nemel mi tady, vidím jak dost tě ten zlobr zřídil.' Tak sakra, kde je ten lékař?!\n"
        + "'Není třeba pane starosto' řekl zaklínač hlastitěji. O toho trola jsem se vám postaral...\n"
        + "'Ach, ty jsi ale tvrdohlavý zaklínač. Tak tedy dobře. Na tady máš svou odměnu.' (získal jsi 100 zlotých)\n";

    public static String TASK_THREE_FINISHED = "Se starostovou dcerou si během cesty moc slov neprohodil. Není také divu, ta dívka se klepala a byla v šoku.'\n" 
        + "Doufal si, že se co nejdřív vrátíš do vesnice, protože slova zaklínače evidentně dívku moc neuklidňovala.\n"
        + "Když byla vesnice na obzoru, odychnul sis. Dokonce ti i přišlo, že dívka se přestala klepat a slyšel jsi tichá slova díku.\n"
        + "Když si přijel do vesnici vzbudil si veliký rozruch. Nikdo nečekal, že bys přivedl starostovu dceru živou.\n"
        + "'Dcerko? Dcerko moje milovaná'.\n"
        + "Pokusil ses odejít trochu stranou, jako zaklínač jsi nerad středem pozornosti všech čumilů.\n"
        + "'Ach zaklínači'. Nevím jak se ti jako otec mohu odměnit, ale jako starosta tě náležitě za tvé činy odměním.'\n"
        + "'Pane starosto, můžeme obchodní stránku věci vyřešit více v soukromí? Z vlastních zkušeností vím, že je to tak nejlepší.'\n"
        + "'Samozřejmě pane zaklínači. Stavte se u mě dnes večer na radnici. A zaklínači... Děkuji, tohle ti nikdy nezapomenu.\n'"
        + "Večer si se stavil na radnici a prohodil si ještě pár slov se starostou. (získal jsi 100 zlotých)\n";

    public static String TASK_FINISHED = "'Zaklínači, všechny naše problémy jsi vyřešil. Bej tebou se jdu po další práci optat na hradě, určitě tam pro tebe něco najdou.'\n" 
        + "'Díky pane starosto, určitě se po práci optám i tam.'";
    
    public static String CASTLE_VISIT_BEFORE_TAKS_FINISH = "Co tady pohledáváš cizáku, jdi pryč než si tam pro tebe dojdu (vrátil jsi se do předešlé lokace).\n---\n";
    
    public static String FIRST_CASTLE_VISIT = "Zaklínač došel k hradu. Prošel hlavní branou a rozhlédl se.\n"
        + "Už sis zvykl, že na tebe nikde nečekají s úsmevěm na tváři a nemyslíš si, že to tady bude jiné.\n"
        + "Od hlavního vchodu se k tobě blížil pan purkrabí a zdálky na tebe volal.\n" 
        + "'Tvá pověst tě předchází pane zaklínači, mluví o vás celá vesnice.'\n"
        + "Pouze si se pousmál očekávaje co bude následovat.\n" 
        + "'I my tady máme problém, který nás trápí. Poslední dobou celou naši zemi sužují požáry, během kterých umírá spoustu lidí.'\n" 
        + "'Pane purkhrabí, neberte to zle, já jsem zaklínač, ne požárník na co by...'\n"
        + "'Na žerty nemám čas.' řekl rozzuřeně pan purkrabí.'Požáry má nasvědomí drak.'\n"
        + "'Pane purkrabí, v našem kodexu máme zakázáno zabíjet draky'\n"
        + "'Na zásady teď není čas, umírají lidé. Na draka jsem poslal část vojska a od vás chci, abyste jim pomohl.'\n"
        + "'Já draky nezabíjím.'\n"
        + "'Chcete snad aby kvůli vaší ješitnosti umírali nevinní lidé? No tak zaklínači, mějte rozum.'" 
        + "'Podívám se na to, nic vám neslibuji.'\n"
        + "---\nJe zpřístupněna finální lokace s drakem. Z této cesty není návratu.\n"
        + "Abys získal přehled o lokaci, doporučuji použít příkaz 'Rozhledni_se'";

    public static String AFTER_FINAL_BATTLE = "Svým posledním úderem jsi draka zasáhnul smrtelnout ránou.\n"
        + "Tvá kůže je rožhavena, divíš se, že stále stojíš na nohou.\n" 
        + "V životě si s drakem nebojoval... A nejspíš už ani nebudeš.";
}
