package dk.witcherhra.game;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Třída představuje nepřítele, proti kterému hráč může bojovat. Nepřátel je více typů, jejich atributy se
 * jsou v {@link Encounters}, tato třída se také používá pro reprezentaci bosse.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 */
public class Enemy implements ICharacters, Cloneable
{
    public static int SPAWN_RATIO = 3; //Cim mensi cislo, tim vetsi sance na spawnuní nepřítele

    private String name;
    private int hpBar; // Aktuální životy encountera
    private int lowerDamage; // Spodní hranice poškození encountera
    private int upperDamage; // Horní hranice poškození encountera
    private int dodgeNumber; // Čím menší, tím lepší a vyšší šance na vyhnutí
    private boolean isBoss; //Je protivník Bossem?
    private boolean isDead; // Je encounter mrtvý?;

    public Enemy(String name, int hpBar, int lowerDamage, int upperDamage, int dodgeNumber) {
        this.name = name;
        this.hpBar = hpBar;
        this.lowerDamage = lowerDamage;
        this.upperDamage = upperDamage;
        this.dodgeNumber = dodgeNumber;
    }

    /**
     * Metoda vrací název nepřítele.
     *
     * @return název nepřítele
     */
    public String getName() {
        return name;
    }

    /**
     * Do HashMap konstanty "enemies" ve tříde "Encounters" jsme si uložili instance různých nepřátel s různými atributy. 
     * Abychom mohli tyto nepřátele použít a zároveň neměnit instance v HashMap, je potřeba při každém vytvoření nepřítele udělat klona.
     * 
     * @return Enemy (objekt nepřítel)
     */
    public Enemy clone() throws CloneNotSupportedException
    {
        return (Enemy)super.clone();
    }

    /**
     * Metoda nastaví nepříteli zdraví.
     *
     * @param hpBar zdraví, co se má nepříteli nastavit
     */
    public void setHpBar(int hpBar) {
        this.hpBar = hpBar;
    }

    /**
     * Metoda vrací nepřítelovo zdraví.
     *
     * @return zdraví nepřítele
     */
    public int getHpBar() {
        if (hpBar < 1) {
            this.isDead = true;
        }

        return hpBar;
    }

    /**
     * Metoda vrací spodní hranici poškození, které může nepřítel udělit.
     *
     * @return spodní hranice poškození nepřítele
     */
    public int getLowerDamage() {
        return lowerDamage;
    }

    /**
     * Metoda vrací horní hranici poškození, které může nepřítel udělit.
     *
     * @return horní hranice poškození nepřítele
     */
    public int getUpperDamage() {
        return upperDamage;
    }

    /**
     * Metoda vrací náhodné poškození (vrámci spodní a horní hranice), které nepřítel udělí.
     *
     * @return poškození, co nepřítel udělí
     */
    public int damageDealed() {
        return ThreadLocalRandom.current().nextInt(lowerDamage, upperDamage + 1);
    }

    /**
     * Metoda náhodně rozhodne, jestli se nepřítel vyhnul hráčovo útoku.
     *
     * @return {@code true}, pokud se nepřítel vyhne útoku; pokud ne tak {@code false}
     */
    public boolean isAttackDodged() {
        boolean isDodged = false;

        if (ThreadLocalRandom.current().nextInt(1, dodgeNumber + 1) == dodgeNumber) {
            isDodged = true;
        }

        return isDodged;
    }

    /**
     * Metoda vrací, jestli je nepřítel mrtvý.
     *
     * @return {@code true}, je nepřítel mrtvý; pokud ne tak {@code false}
     */
    public boolean isDead() {
        return isDead;
    }

    /**
     * Metoda vrací, jestli je nepřítel boss.
     *
     * @param isBoss je nepřítel boss
     */
    public void setIsBoss(boolean isBoss) {
        this.isBoss = isBoss;
    }

    /**
     * Metoda vrací, jestli je nepřítel boss.
     *
     * @return {@code true}, je nepřítel boss; pokud ne tak {@code false}
     */
    public boolean isBoss() {
        return isBoss;
    }

    /**
     * Metoda vrací odměnu po zabití nepřítele.
     *
     * @return odměna pro hráče po zabití nepřítele
     */
    public int getRewardAfterKillEnemy() {
        return ThreadLocalRandom.current().nextInt(15, 25);
    }

    /**
     * Metoda vrací odměnu po zabití bosse.
     *
     * @return odměna pro hráče po zabití bosse
     */
    public int getRewardAfterKillBoss() {
        return ThreadLocalRandom.current().nextInt(45, 75);
    }

    /**
     * Metoda vrací kompletní informace o protivníkovi. Výsledek volání obsahuje:
     * <ul>
     *     <li>jméno protivníka</li>
     *     <li>aktuální počet životů</li>
     *     <li>poškození</li>
     *     <li>šance na vyhnutí</li>
     * </ul>
     *
     * @return kompletní informace o nepříteli
     */
    public String getDescription()
    {
        return "Jméno protivníka: '" + name + "'\n" 
        + "Aktuální počet životů: " + hpBar + "HP\n"
        + "Poškození: " + lowerDamage + " - " + upperDamage + "\n"
        + "Šance na vyhnutí (čím vyšší číslo, tím menší šance): " + dodgeNumber;
    }

}
