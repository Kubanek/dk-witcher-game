package dk.witcherhra.game;

/**
 * Třída obsahuje statické informace o místnostech hry.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class GameLocation
{

    public static String CURRENT_AREA = "Aktuální pozice: ";
    public static String EXITS = "Východy: ";
    public final static String FALL_BACK = "Vratit se do předešlé lokace";

    public static String DIRECTION_NORTH = "Sever";
    public static String DIRECTION_SOUTH = "Jih";
    public static String DIRECTION_EAST = "Vychod";
    public static String DIRECTION_WEST = "Zapad";

    public static String VILLAGE = "Vesnice";
    public static String VILLAGE_DESCRIPTION = "Nacházíš se ve vesnici, zde vidíš Hostinec, Nevěstinec a Kováře.";
    
    public static String PUB = "Hostinec";
    public static String PUB_DESC = "V hospodě můžeš přespat (příkaz 'Vyspi_se') a doplnit si maximální HP a léčicí elixíry. Cena za noc je 50 zlotých";
    public static String PUB_AFTER_SLEEPING = "Měkká postel, jídlo a pivo pod nos, to si necháš líbit. Po přespání v hospodě se cítíš plných sil (automaticky se vám doplnili maximální HP a vaše léčicí elixíry).\n";
    public static int PUB_COST = 50;
    
    public static String BROTHEL = "Nevestinec";
    public static String BROTHEL_DESC = "V nevěstinci si můžes odpočinout (příkaz 'Odpocin_si') a doplnit si 40 HP. Cena je 20 zlotých";
    public static String BROTHEL_AFTER_HAVING_FUN = "Návštěva nevěstnice se ti vždycky líbí. Skoro není lepší společnosti, tedy až na Klepnu (doplnilo se vám 40 HP).\n";
    public static int BROTHEL_COST = 20;
    public static int BROTHEL_HEALING = 40;
    
    public static String BLACKSMITH = "Kovarna";
    public static String BLACKSMITH_DESCRIPTION = "U Kováře si můžeš nechat vylepšít zaklínačské vybavení (prikaz 'Vylepsi') :\n"
        + Inventory.SWORD + " [poškození: " + Inventory.UPRADGED_LOWER_DAMAGE + " - " + Inventory.UPRADGED_UPPER_DAMAGE + "]" + " - " + Inventory.SWORD_COST + " zlotých\n" 
        + Inventory.ARMOR + " [maximální HP: " + Inventory.UPRADGED_MAX_HP + ", vyhnutí: " + Inventory.UPRADGED_DODGE_NUMBER + "]" + " - " + Inventory.ARMOR_COST +  " zlotých\n"
        + Inventory.POTION_SAC + " [počet elixírů: " + Inventory.UPRADGED_MAXIMUM_OF_POTIONS +  "]" + " - " + Inventory.POTION_SAC_COST  + " zlotých";
    public static String BLACKSMITH_UPRADGE = "Je potřeba kovářovi říct, jaké vybavení má vylepšit (" + Inventory.SWORD + ", " + Inventory.ARMOR + ", "  + Inventory.POTION_SAC + ").";

    public static String WAY = "Cesta";
    public static String WAY_START_DESCRIPTION = "Tato cesta spojuje vesnici s hradem. Jsi na jejím začátku.";
    public static String WAY_MIDDLE_DESCRIPTION = "Cesta spojujici vesnici s hradem.";
    public static String WAY_END_DESCRIPTION = "Tato cesta spojuje vesnici s hradem. Na severu vidíš velký hrad.";
    
    public static String OPEN_SPACE_FIELD = "Planina";
    public static String OPEN_SPACE_FIELD_DESCRIPTION = "Před tebou se rozkládá obrovská planina.";
    
    public static String CORNFIELD = "Pole s obilim";
    public static String CORNFIELD_DESCRIPTION = "Nacházíš se v rozsáhlém poli, které patří vesnici";
    
    public static String FOREST = "Les";
    public static String FOREST_DESCRIPTION = "Nacházíš se v lese, zatím není tak hustý.";
    
    public static String DEEP_FOREST = "Hluboky les";
    public static String DEEP_FOREST_DESCRIPTION = "Nacházíš se v lese hlubokém lese";
    
    public static String ALLEY_WITH_TREES = "Alej se stromy";
    public static String ALLEY_WITH_TREES_DESCRIPTION = "Tyto áleje patří vesnici. Říká se, že z těchto álejí pochází ta nejlepší jablka.";
    
    public static String BURIAL_GROUD = "Pohrebiste";
    public static String BURIAL_GROUD_DESCRIPTION = "Zde tě čeká snad už jenom smrt. Skrytě doufáš, že jako zaklínač se sem tvá mrtvola nikdy nedostane.";
    
    public static String SIGNPOST = "Rozcestnik";
    public static String SIGNPOST_DESC = "Odtud můžeš zamířit do celého herního světa";
    
    public static String CASTLE = "Hrad";
    public static String CASTLE_DESC = "Obrovský hrad s minimálně jedním plukem vojáků.";

    public static String BANDIT_CAMP = "Taboriste banditu";
    public static String BANDIT_CAMP_DESC = "Taboriste hrdlozeřů, banditů a různých lapků. Pro běžného člověka je toto místo hotová noční můra";
    
    public static String AGGRESSIVE_TROLL_CAVE = "Jeskyne agresivniho trola";
    public static String AGGRESSIVE_TROLL_CAVE_DESC = "Kotel a obrovská jeskyně za ním. Jedno víš jistě, rozhodně nechceš skončit v tom kotli.";
    
    public static String KIDNAPPER_WITH_MAYORS_DAUGHTER = "Unosce se starostovou dcerou";
    public static String KIDNAPPER_WITH_MAYORS_DAUGHTER_DESC = "Vidíš malý povoz s koňmi. Dle čoudícího ohně soudíš, že majitel povozu je poblíž.";
    
    public static String DRAGON_CAVE = "Jeskyne_s_drakem";
    public static String DRAGON_CAVE_DESC = "Finální destinace, jeskyně s drakem";
    
    public static String IN_LOCATION_IS_NOTHING_TO_INVESTIGATE = "V lokaci se nenachází nic zajímavého na prozkoumání.";
    public static String FAILED_PREVIOUS_LOCATION_CHANGE = "Z minulé lokace ses přesunul z nějakého důvodu. Ani tě nenapadne se tam vracet.";
    public static String FAILED_CURRENT_LOCATION_CHANGE = "Nemůžeš odejít z této lokace, protože se v lokaci nachází protivník.";
    
    public static String NOBODY_TO_FIGHT = "V lokaci se nenachází nikdo, s kým bys mohl bojovat";
    
    public static String THIS_ITEM_CANNOT_BE_UPRADGED_AGAIN = "Tenhle předmět byl už jednou vylepšen, nelze jej vylepšit znova.";

    public static double VILLAGE_LEFT = 130.0;
    private static final double STARTING_MAP_POSITION = 320.0;
    private static final double MAP_COLUMN_MULTIPLIER = 0.59;

    public static double FIRST_COLUMN_MAP = STARTING_MAP_POSITION;
    public static double SECOND_COLUMN_MAP = STARTING_MAP_POSITION * (1 + MAP_COLUMN_MULTIPLIER);
    public static double THIRD_COLUMN_MAP = STARTING_MAP_POSITION * (1 + (MAP_COLUMN_MULTIPLIER * 2));
    public static double FOURTH_COLUMN_MAP = STARTING_MAP_POSITION * (1 + (MAP_COLUMN_MULTIPLIER * 3));
    public static double FIFTH_COLUMN_MAP = STARTING_MAP_POSITION * (1 + (MAP_COLUMN_MULTIPLIER * 4));

    private static final double TOP_POSITION = 32;
    private static final double ROW = 56.25;

    public static double FIRST_ROW = TOP_POSITION;
    public static double SECOND_ROW = TOP_POSITION + ROW;
    public static double THIRD_ROW = TOP_POSITION + (ROW * 2);
    public static double FOURTH_ROW = TOP_POSITION + (ROW * 3);
    public static double FIFTH_ROW = TOP_POSITION + (ROW * 4);
    public static double SIXTH_ROW = TOP_POSITION + (ROW * 5);
    public static double SEVENTH_ROW = TOP_POSITION + (ROW * 6);
}
