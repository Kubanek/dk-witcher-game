package dk.witcherhra.game;

/**
 * Třída implementující předměty. Ve třdě se nachází všechny metody spojené
 * s předměty. Ve třídě se nachází také statické hodnoty spojené
 * s předměty.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 */
public class Item implements Comparable<Item>
{

    private String name;
    private String description;
    private boolean moveable;
    private int healingNumber;

    public static int STANDARD_HEALING_NUMBER = 10;
    public static String BEER = "Pivo";
    public static String BEER_DESC = "Tohle sis přesně přál, krásně točené pivko a to nejlepší je, že je na účet podniku.";

    public static String VODKA = "Vodka";
    public static String VODKA_DESC = "Vodka. Jde vidět, že hostinský už pár zaklínačů obsluhoval. Na účet podniku.";
    public static int VODKA_HEALING_NUMBER = 15;

    public static String CHICKEN = "Kure";
    public static String CHICKEN_DESC = "Na účet podniku ti bylo objednáno i kuře.";
    public static int CHICKEN_HEALING_NUMBER = 15;

    public static String APPLE = "Jablko";
    public static String APPLE_DESC = "Šťavnaté červené jablko";

    public static String PEAR = "Hruska";
    public static String PEAR_DESC = "Zelená sladká hruška";

    public static String MEAT = "Maso";
    public static String MEAT_DESC = "Není to sice pořádně propečený steak, ale i tak si na něm pochutnáš";

    public static int MEAT_HEALING_NUMBER = 20;

    public static String BONES = "Kosti";
    public static String BONES_DESC = "Nejspíše kosti jedné z drakových obětí.";

    public static String MUSHROOMS = "Houby";
    public static String MUSHROOMS_DESC = "Tyhle houby vidíš prvně, ale připadají ti k snětku";

    public static String CHAIR = "Zidle";
    public static String CHAIR_DESC = "Krásná hospodká židle se 3 nohama. Ta už asi něco zažila.";

    public static String CORPSE = "Mrtvola";
    public static String CORPSE_ONE_DESC = "Nejspíše místní sedlák zabitý sečným úderem přes obličej.";
    public static String CORPSE_TWO_DESC = "Tahle oběť byla zabita již před dlouhou dobou, nedovedeš rozpoznat tělo oběti.";

    public static String CORPSES = "Mrtvoly";
    public static String CORPSES_DESC = "Hromada mrtvol, nedovedeš rozeznat . Nejspíšě všichni umřeli pod rukama banditů";

    /**
     * Konstruktor třídy, vytvoří daný předmět, nastaví atributy.
     */
    public Item(String name, String description, boolean moveable, int healingNumber)
    {
        this.name = name;
        this.description = description;
        this.moveable = moveable;
        this.healingNumber = healingNumber;
    }

    /**
     * Konstruktor třídy, vytvoří daný předmět, nastaví atributy.
     */
    public Item(String name, String description)
    {
        this(name, description, false, 0);
    }

    /**
     * Metoda vrácí nazev předmětu.
     *
     * @return název předmětu
     */
    public String getName()
    {
        return name;
    }

    /**
     * Metoda vrácí popis předmětu.
     *
     * @return popis předmětu
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Vrací kolik HP se obnoví použitím předmětu (např healing lektvar nebo pivo).
     *
     * @return počet HP k obnovení
     */
    public int getHealingNumber()
    {
        return healingNumber;
    }

    /**
     * Metoda vrací informaci o tom, zda je předmět sebratelný.
     *
     * @return {@code true}, pokud je předmět sebratelný; jinak {@code false}
     */
    public boolean isMoveable()
    {
        return moveable;
    }

    /**
     * Metoda vrácí popis celého předmětu, tedy název, počet doplněných životů a popis.
     *
     * @return popis předmětu
     */
    public String getFullDescription() {
        return name + "[HP: " + healingNumber + "]" + " - " + description;
    }

    /**
     * Metoda porovnává dva předměty <i>(objekty)</i>. Lokace jsou shodné,
     * pokud mají stejný název <i>(atribut {@link #name})</i>. Tato metoda
     * je důležitá pro správné fungování seznamu východů do sousedních
     * lokací.
     * <p>
     * Podrobnější popis metody najdete v dokumentaci třídy {@linkplain Object}.
     *
     * @param o objekt, který bude porovnán s aktuálním
     * @return {@code true}, pokud mají obě lokace stejný název; jinak {@code false}
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o)
    {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o instanceof Item) {
            Item item = (Item) o;

            return name.equals(item.getName());
        }

        return false;
    }

    /**
     * Metoda vrací číselný identifikátor instance, který se používá
     * pro optimalizaci ukládání v dynamických datových strukturách.
     * Při překrytí metody {@link #equals(Object) equals} je vždy nutné překrýt i tuto
     * metodu.
     * <p>
     * Podrobnější popis pravidel pro implementaci metody najdete
     * v dokumentaci třídy {@linkplain Object}.
     *
     * @return číselný identifikátor instance
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    @Override
    public int compareTo(Item item)
    {
        return name.compareTo(item.getName());
    }
}
