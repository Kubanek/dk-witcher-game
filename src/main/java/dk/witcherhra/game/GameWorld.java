package dk.witcherhra.game;

import dk.witcherhra.game.action.ActionFallBack;
import dk.witcherhra.game.action.ActionMove;
import dk.witcherhra.util.Observer;
import dk.witcherhra.util.SubjectOfChange;

import java.util.HashSet;
import java.util.Set;

/**
 * Třída představující mapu lokací herního světa. V datovém atributu
 * {@link #currentArea} uchovává odkaz na aktuální lokaci, ve které
 * se hráč právě nachází. Z aktuální lokace je možné se prostřednictvím
 * jejích sousedů dostat ke všem přístupným lokacím ve hře.
 * <p>
 * Veškeré informace o stavu hry <i>(mapa prostorů, inventář, vlastnosti
 * hlavní postavy, informace o plnění úkolů apod.)</i> by měly být uložené
 * zde v podobě datových atributů.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-23
 */

public class GameWorld implements SubjectOfChange {
    public static int lastAreaId = 0;

    private Area currentArea;
    private Area previousArea;
    private String gameState;

    private final Set<Observer> observers = new HashSet<>();

    /**
     * Konstruktor třídy, vytvoří jednotlivé lokace a propojí je pomocí východů. Taktéž nastavuje jednolivé položky na mapě
     */
    public GameWorld() {
        Area village = new Area(GameLocation.VILLAGE, GameLocation.VILLAGE_DESCRIPTION, GameLocation.FOURTH_ROW, GameLocation.VILLAGE_LEFT);
        Area brothel = new Area(GameLocation.BROTHEL, GameLocation.BROTHEL_DESC, GameLocation.FOURTH_ROW, GameLocation.VILLAGE_LEFT);
        Area pub = new Area(GameLocation.PUB, GameLocation.PUB_DESC, GameLocation.FOURTH_ROW, GameLocation.VILLAGE_LEFT);
        Area blacksmith = new Area(GameLocation.BLACKSMITH, GameLocation.BLACKSMITH_DESCRIPTION, GameLocation.FOURTH_ROW, GameLocation.VILLAGE_LEFT);
        Area signpost = new Area(GameLocation.SIGNPOST, GameLocation.SIGNPOST_DESC, GameLocation.FOURTH_ROW, GameLocation.FIRST_COLUMN_MAP);

        Area castle = new Area(GameLocation.CASTLE, GameLocation.CASTLE_DESC, GameLocation.SECOND_ROW, GameLocation.FOURTH_COLUMN_MAP);

        Area banditCamp = new Area(GameLocation.BANDIT_CAMP, GameLocation.BANDIT_CAMP_DESC, GameLocation.FIRST_ROW, GameLocation.SECOND_COLUMN_MAP);
        banditCamp.setBoss(Bosses.getBoss(Bosses.BANDIT_BOSS));

        Area trollCave = new Area(GameLocation.AGGRESSIVE_TROLL_CAVE, GameLocation.AGGRESSIVE_TROLL_CAVE_DESC, GameLocation.SEVENTH_ROW, GameLocation.SECOND_COLUMN_MAP);
        trollCave.setBoss(Bosses.getBoss(Bosses.TROLL_BOSS));

        Area kidnapper = new Area(GameLocation.KIDNAPPER_WITH_MAYORS_DAUGHTER, GameLocation.KIDNAPPER_WITH_MAYORS_DAUGHTER_DESC, GameLocation.SIXTH_ROW, GameLocation.FOURTH_COLUMN_MAP);
        kidnapper.setBoss(Bosses.getBoss(Bosses.KIDNAPPER_BOSS));

        Area dragonCave = new Area(GameLocation.DRAGON_CAVE, GameLocation.DRAGON_CAVE_DESC, GameLocation.FIRST_ROW, GameLocation.FOURTH_COLUMN_MAP);
        dragonCave.setBoss(Bosses.getBoss(Bosses.DRAGON_BOSS));

        Area wayOne = new Area(GameLocation.WAY, GameLocation.WAY_START_DESCRIPTION, true, GameLocation.FOURTH_ROW, GameLocation.SECOND_COLUMN_MAP);
        Area wayTwo = new Area(GameLocation.WAY, GameLocation.WAY_MIDDLE_DESCRIPTION, true, GameLocation.FOURTH_ROW, GameLocation.THIRD_COLUMN_MAP);
        Area wayThree = new Area(GameLocation.WAY, GameLocation.WAY_MIDDLE_DESCRIPTION, true, GameLocation.THIRD_ROW, GameLocation.THIRD_COLUMN_MAP);
        Area wayFour = new Area(GameLocation.WAY, GameLocation.WAY_END_DESCRIPTION, true, GameLocation.THIRD_ROW, GameLocation.FOURTH_COLUMN_MAP);

        Area cornfieldOne = new Area(GameLocation.CORNFIELD, GameLocation.CORNFIELD_DESCRIPTION, true,  GameLocation.THIRD_ROW, GameLocation.FIRST_COLUMN_MAP);
        Area cornfieldTwo = new Area(GameLocation.CORNFIELD, GameLocation.CORNFIELD_DESCRIPTION, true,  GameLocation.THIRD_ROW, GameLocation.SECOND_COLUMN_MAP);

        Area openSpaceFieldOne = new Area(GameLocation.OPEN_SPACE_FIELD, GameLocation.OPEN_SPACE_FIELD_DESCRIPTION, true,  GameLocation.FIFTH_ROW, GameLocation.FIRST_COLUMN_MAP);
        Area openSpaceFieldTwo = new Area(GameLocation.OPEN_SPACE_FIELD, GameLocation.OPEN_SPACE_FIELD_DESCRIPTION, true, GameLocation.SECOND_ROW, GameLocation.FIRST_COLUMN_MAP);
        Area openSpaceFieldThree = new Area(GameLocation.OPEN_SPACE_FIELD, GameLocation.OPEN_SPACE_FIELD_DESCRIPTION, true, GameLocation.FIRST_ROW, GameLocation.FIRST_COLUMN_MAP);

        Area forestOne = new Area(GameLocation.FOREST, GameLocation.FOREST_DESCRIPTION, true, GameLocation.FIFTH_ROW, GameLocation.THIRD_COLUMN_MAP);
        Area forestTwo = new Area(GameLocation.FOREST, GameLocation.FOREST_DESCRIPTION, true, GameLocation.SECOND_ROW, GameLocation.SECOND_COLUMN_MAP);
        Area forestThree = new Area(GameLocation.FOREST, GameLocation.FOREST_DESCRIPTION, true, GameLocation.SIXTH_ROW, GameLocation.SECOND_COLUMN_MAP);

        Area deepForestOne = new Area(GameLocation.DEEP_FOREST, GameLocation.DEEP_FOREST_DESCRIPTION, true, GameLocation.SIXTH_ROW, GameLocation.THIRD_COLUMN_MAP);
        Area deepForestTwo = new Area(GameLocation.DEEP_FOREST, GameLocation.DEEP_FOREST_DESCRIPTION, true, GameLocation.FIFTH_ROW, GameLocation.FOURTH_COLUMN_MAP);

        Area alleyOne = new Area(GameLocation.ALLEY_WITH_TREES, GameLocation.ALLEY_WITH_TREES_DESCRIPTION, true, GameLocation.SIXTH_ROW, GameLocation.FIRST_COLUMN_MAP);

        Area burialGround = new Area(GameLocation.BURIAL_GROUD, GameLocation.BURIAL_GROUD_DESCRIPTION, GameLocation.THIRD_ROW, GameLocation.FIFTH_COLUMN_MAP);

        village.addExit(brothel);
        village.addExit(pub);
        village.addExit(blacksmith);
        village.addExit(signpost);

        brothel.addExit(pub);
        brothel.addExit(blacksmith);
        brothel.addExit(signpost);

        pub.addExit(brothel);
        pub.addExit(blacksmith);
        pub.addExit(signpost);

        blacksmith.addExit(pub);
        blacksmith.addExit(brothel);
        blacksmith.addExit(signpost);

        //rozcesti
        signpost.addExit(village);
        signpost.addExit(cornfieldOne, GameLocation.DIRECTION_NORTH);
        signpost.addExit(wayOne, GameLocation.DIRECTION_EAST);
        signpost.addExit(openSpaceFieldOne, GameLocation.DIRECTION_SOUTH);

        //cesta1
        wayOne.addExit(signpost, GameLocation.DIRECTION_WEST);
        wayOne.addExit(cornfieldTwo, GameLocation.DIRECTION_NORTH);
        wayOne.addExit(wayTwo, GameLocation.DIRECTION_EAST);

        //cesta2
        wayTwo.addExit(wayOne, GameLocation.DIRECTION_WEST);
        wayTwo.addExit(wayThree, GameLocation.DIRECTION_NORTH);
        wayTwo.addExit(forestOne, GameLocation.DIRECTION_SOUTH);

        //cesta3
        wayThree.addExit(cornfieldTwo, GameLocation.DIRECTION_WEST);
        wayThree.addExit(wayFour, GameLocation.DIRECTION_EAST);
        wayThree.addExit(wayTwo, GameLocation.DIRECTION_SOUTH);

        //cesta4
        wayFour.addExit(wayThree, GameLocation.DIRECTION_WEST);
        wayFour.addExit(castle);
        wayFour.addExit(burialGround, GameLocation.DIRECTION_EAST);

        castle.addExit(wayFour);
        castle.addExit(dragonCave);

        burialGround.addExit(wayFour, GameLocation.DIRECTION_WEST);

        //pole1
        cornfieldOne.addExit(openSpaceFieldTwo, GameLocation.DIRECTION_NORTH);
        cornfieldOne.addExit(cornfieldTwo, GameLocation.DIRECTION_EAST);
        cornfieldOne.addExit(signpost, GameLocation.DIRECTION_SOUTH);

        //pole2
        cornfieldTwo.addExit(cornfieldOne, GameLocation.DIRECTION_WEST);
        cornfieldTwo.addExit(forestTwo, GameLocation.DIRECTION_NORTH);
        cornfieldTwo.addExit(wayThree, GameLocation.DIRECTION_EAST);
        cornfieldTwo.addExit(wayOne, GameLocation.DIRECTION_SOUTH);

        //planina 2
        openSpaceFieldTwo.addExit(openSpaceFieldThree, GameLocation.DIRECTION_NORTH);
        openSpaceFieldTwo.addExit(forestTwo, GameLocation.DIRECTION_EAST);
        openSpaceFieldTwo.addExit(cornfieldOne, GameLocation.DIRECTION_SOUTH);

        //les 2
        forestTwo.addExit(openSpaceFieldTwo, GameLocation.DIRECTION_WEST);
        forestTwo.addExit(banditCamp, GameLocation.DIRECTION_NORTH);
        forestTwo.addExit(cornfieldTwo, GameLocation.DIRECTION_SOUTH);

        //planina 3
        openSpaceFieldThree.addExit(banditCamp, GameLocation.DIRECTION_EAST);
        openSpaceFieldThree.addExit(openSpaceFieldTwo, GameLocation.DIRECTION_SOUTH);

        banditCamp.addExit(openSpaceFieldThree, GameLocation.DIRECTION_WEST);
        banditCamp.addExit(forestTwo, GameLocation.DIRECTION_SOUTH);

        //planina 1
        openSpaceFieldOne.addExit(signpost, GameLocation.DIRECTION_NORTH);
        openSpaceFieldOne.addExit(alleyOne, GameLocation.DIRECTION_SOUTH);

        //aleje 1 
        alleyOne.addExit(openSpaceFieldOne, GameLocation.DIRECTION_NORTH);
        alleyOne.addExit(forestThree, GameLocation.DIRECTION_EAST);

        //les 3 
        forestThree.addExit(alleyOne, GameLocation.DIRECTION_WEST);
        forestThree.addExit(deepForestOne, GameLocation.DIRECTION_EAST);
        forestThree.addExit(trollCave, GameLocation.DIRECTION_SOUTH);

        trollCave.addExit(forestThree, GameLocation.DIRECTION_NORTH);

        //Hluboky les 1
        deepForestOne.addExit(forestThree, GameLocation.DIRECTION_WEST);
        deepForestOne.addExit(forestOne, GameLocation.DIRECTION_NORTH);
        deepForestOne.addExit(kidnapper, GameLocation.DIRECTION_EAST);

        kidnapper.addExit(deepForestOne, GameLocation.DIRECTION_WEST);
        kidnapper.addExit(deepForestTwo, GameLocation.DIRECTION_NORTH);

        //Hluboky les 2
        deepForestTwo.addExit(forestOne, GameLocation.DIRECTION_WEST);
        deepForestTwo.addExit(kidnapper, GameLocation.DIRECTION_SOUTH);

        //les 1
        forestOne.addExit(wayTwo, GameLocation.DIRECTION_NORTH);
        forestOne.addExit(deepForestTwo, GameLocation.DIRECTION_EAST);
        forestOne.addExit(deepForestOne, GameLocation.DIRECTION_SOUTH);

        Item beer = new Item(Item.BEER, Item.BEER_DESC, true, Item.STANDARD_HEALING_NUMBER);
        Item vodka = new Item(Item.VODKA, Item.VODKA_DESC, true, Item.VODKA_HEALING_NUMBER);
        Item chicken = new Item(Item.CHICKEN, Item.CHICKEN_DESC, true, Item.CHICKEN_HEALING_NUMBER);
        Item chair = new Item(Item.CHAIR, Item.CHAIR_DESC);
        Item meet = new Item(Item.MEAT, Item.MEAT_DESC, true, Item.MEAT_HEALING_NUMBER);
        Item bones = new Item(Item.BONES, Item.BONES_DESC);

        pub.addItem(beer);
        pub.addItem(vodka);
        pub.addItem(chicken);
        pub.addItem(chair);

        dragonCave.addItem(meet);
        dragonCave.addItem(bones);

        Item apple = new Item(Item.APPLE, Item.APPLE_DESC, true, Item.STANDARD_HEALING_NUMBER);
        Item pear = new Item(Item.PEAR, Item.PEAR_DESC, true, Item.STANDARD_HEALING_NUMBER);

        alleyOne.addItem(apple);
        alleyOne.addItem(pear);

        Item mushrooms = new Item(Item.MUSHROOMS, Item.MUSHROOMS_DESC, true, Item.STANDARD_HEALING_NUMBER);

        burialGround.addItem(mushrooms);

        Item corpseOne = new Item(Item.CORPSE, Item.CORPSE_ONE_DESC);
        Item corpseTwo = new Item(Item.CORPSE, Item.CORPSE_TWO_DESC);
        Item corpses = new Item(Item.CORPSES, Item.CORPSES_DESC);

        cornfieldTwo.addItem(corpseOne);
        forestTwo.addItem(corpses);
        openSpaceFieldThree.addItem(corpseTwo);

        currentArea = village;
        previousArea = village;
    }

    /**
     * Metoda vrací odkaz na aktuální lokaci, ve které se hráč právě nachází.
     *
     * @return aktuální lokace
     */
    public Area getCurrentArea() {
        return currentArea;
    }

    /**
     * Metoda nastaví aktuální lokaci. Používá ji příkaz {@link ActionMove}
     * při přechodu mezi lokacemi.
     *
     * @param currentArea lokace, která bude nastavena jako aktuální
     */
    public void setCurrentArea(Area currentArea) {
        this.currentArea = currentArea;
        notifyObservers();
    }

    /**
     * Metoda vrací odkaz na přechozí lokaci, ve které se hráč nacházel.
     *
     * @return aktuální lokace
     */
    public Area getPreviousArea() {
        return previousArea;
    }

    /**
     * Metoda nastaví předchozí lokaci. Používá ji příkaz {@link ActionFallBack}
     * při útěku z aktuální lokace.
     *
     * @param previousArea lokace, která bude nastavena jako aktuální
     */
    public void setPreviousArea(Area previousArea) {
        this.previousArea = previousArea;
        notifyObservers();
    }

    /**
     * Metoda vrací aktuální stav hry <i>(běžící hra, výhra, prohra)</i>.
     *
     * @return aktuální stav hry
     */
    public GameState getGameState() {
        if (currentArea.getName().equals(GameLocation.DRAGON_CAVE) && !currentArea.hasEnemy()) {
            return GameState.WON;
        }

        return GameState.PLAYING;
    }

    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}