package dk.witcherhra.game;

import dk.witcherhra.util.Observer;
import dk.witcherhra.util.SubjectOfChange;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Třída reprezentující postavu hráče - Zaklínače.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class Witcher implements ICharacters, SubjectOfChange
{

    public static String WITCHER = "Zaklínač";
    public static int WITCHER_HP = 100;
    public static int WITCHER_LOWER_DAMAGE = 10;
    public static int WITCHER_UPPER_DAMAGE = 20;
    public static int WITCHER_DODGE_NUMBER = 5;

    private int maxHp; // Maximální životy zaklínače
    private int hpBar; // Aktuální životy zaklínače
    private int lowerDamage; // Spodní hranice poškození zaklínače
    private int upperDamage; // Horní hranice poškození zaklínače
    private int dodgeNumber; // Čím menší, tím lepší a vyšší šance na vyhnutí
    private boolean isDead; // Je zaklínač mrtvý?

    private final Set<Observer> observers = new HashSet<>();

    /**
     * Konstruktor třídy witcher.
     */
    public Witcher() 
    {
        this.maxHp = WITCHER_HP;
        this.hpBar = WITCHER_HP;
        this.lowerDamage = WITCHER_LOWER_DAMAGE;
        this.upperDamage = WITCHER_UPPER_DAMAGE;
        this.dodgeNumber = WITCHER_DODGE_NUMBER;
    }

    /**
     * Metoda nastavuje aktuální životy postavy.
     *
     * @param maxHp, maximální počet životů
     */
    public void setMaxHp(int maxHp)
    {
        this.maxHp = maxHp;
        notifyObservers();
    }

    /**
     * Vrátí aktuální maximální počet života postavy
     *
     * @return maximální životy postavy
     */
    public int getMaxHP()
    {
        return maxHp;
    }

    /**
     * Metoda nastavuje aktuální životy postavy.
     *
     * @param hpBar
     */
    public void setHpBar(int hpBar)
    {
        this.hpBar = hpBar;
        notifyObservers();
    }

    /**
     * Metoda nastavuje životy po regeneraci.
     *
     * @param healingNumber počet životů k uzdravení
     */
    public void setHpBarAfterHealing(int healingNumber)
    {
        int hpBar = this.hpBar + healingNumber;

        if (hpBar > maxHp) {
            this.hpBar = maxHp;
        } else {
            this.hpBar = hpBar;
        }

        notifyObservers();
    }

    /**
     * Vrátí aktuální počet života postavy. Pokud zdraví klesne pod hodnotu 1, hra končí, protože je zaklínač mrtev.
     *
     * @return aktuální životy postavy
     */
    public int getHpBar()
    {
        if (hpBar < 1) {
            this.isDead = true;
        }

        return hpBar;
    }

    /**
     * Metoda nastavuje postavě spodní hranici poškození.
     *
     * @param lowerDamage spodní hranice poškození
     */
    public void setLowerDamage(int lowerDamage)
    {
        this.lowerDamage = lowerDamage;
        notifyObservers();
    }

    /**
     * Metoda vrátí nejmenší možné poškození postavy, co postava za útok může napáchat.
     *
     * @return spodní poškození postavy
     */
    public int getLowerDamage()
    {
        return lowerDamage;
    }

    /**
     * Nastavuje postavě horní hranici poškození.
     *
     * @param upperDamage horní hranice poškození
     */
    public void setUpperDamage(int upperDamage)
    {
        this.upperDamage = upperDamage;
        notifyObservers();
    }

    /**
     * Metoda vrátí nejvyšší možné poškození postavy, co postava za útok může napáchat
     *
     * @return horní hranice poškození
     */
    public int getUpperDamage()
    {
        return upperDamage;
    }

    /**
     * Metoda nastavuje číslo vyhnutí.
     * Platí pravidlo, že čím meší číslo, tím větší šance na vyhnutí.
     *
     * @param dodgeNumber číslo pravděpodobnosti vyhnutí se útoku
     */
    public void setDodgeNumber(int dodgeNumber)
    {
        this.dodgeNumber = dodgeNumber;
        notifyObservers();
    }

    /**
     * Metoda vrátí aktuální číselné poškození postavy za dané kolo.
     *
     * @return poškození udělené postavou
     */
    public int damageDealed()
    {
        return ThreadLocalRandom.current().nextInt(lowerDamage, upperDamage + 1);
    }

    /**
     * Metoda vráti hodnotu ano, ne, zda se postava v tomhle kole útoku vyhnula.
     *
     * @return {@code true}, pokud se postava vyhne útoku; pokud ne tak {@code false}
     */
    public boolean isAttackDodged()
    {
        boolean isDodged = false;

        if (ThreadLocalRandom.current().nextInt(1, dodgeNumber + 1) == dodgeNumber) {
            isDodged = true;
        }

        return isDodged;
    }

    /**
     * Metoda vrátí, zda je postava mrtvá,.
     *
     * @return {@code true}, pokud je postava mrtvá; pokud ne tak {@code false}
     */
    public boolean isDead() {
        return isDead;
    }

    /**
     * Metoda nastavuje, že je postava mrtvá.
     */
    public void setIsDead() {
        this.setHpBar(0);
        this.isDead = true;

        notifyObservers();
    }

    /**
     * Metoda vrací sestavený text - kompletní popis postavy.
     *
     * @return kompletní informace o postavě
     */
    public String getDescription()
    {
        return "Aktuální počet zaklínačových životů: " + getHpBar() + "HP z " + maxHp + "HP\n"
        + "Poškození: " + lowerDamage + " - " + upperDamage + "\n"
        + "Šance na vyhnutí (čím vyšší číslo, tím menší šance): " + dodgeNumber;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
