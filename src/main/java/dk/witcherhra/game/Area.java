package dk.witcherhra.game;

import dk.witcherhra.util.Observer;
import dk.witcherhra.util.SubjectOfChange;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Třída představuje lokaci <i>(místo, místnost, prostor)</i> ve scénáři hry.
 * Každá lokace má název, který ji jednoznačně identifikuje. Může mít sousední
 * lokace, do kterých z ní lze odejít. Odkazy na všechny sousední lokace
 * jsou uložené v kolekci. Lokace také může obsahovat předměty. Odkazy na
 * všechny předměty v lokaci jsou uložené v mapě.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class Area implements Comparable<Area>, SubjectOfChange
{
    private int id;

    private String name;
    private String description;
    private boolean canSpawnEnemy;
    private Map<String, Area> exits;
    private Map<String, Item> items;
    private double positionTop;
    private double positionLeft;

    private Enemy enemy;
    private final Set<Observer> observers = new HashSet<>();

    /**
     * Konstruktor třídy, vytvoří lokaci se zadaným názvem, popisem, vrchní a levou souřadnicí.
     *
     * @param name název lokace <i>(jednoznačný identifikátor, musí se jednat o text bez mezer)</i>
     * @param description podrobnější popis lokace
     * @param positionTop vrchní souřadníce
     * @param positionLeft levá souřadníce
     */
    public Area(String name, String description, Double positionTop, Double positionLeft)
    {
        this.id = ++GameWorld.lastAreaId;

        this.name = name;
        this.description = description;
        this.positionTop = positionTop;
        this.positionLeft = positionLeft;

        this.exits = new HashMap<>();
        this.items = new TreeMap<>();
    }

    /**
     * Konstruktor třídy, vytvoří lokaci se zadaným názvem, popisem, informaci zda se do lokace může spawnout nepřítel,
     * vrchní a levou souřadnicí.
     *
     * @param name název lokace <i>(jednoznačný identifikátor, musí se jednat o text bez mezer)</i>
     * @param description podrobnější popis lokace
     * @param canSpawnEnemy Nastavuje, zda se v lokaci může objevit náhodný nepřítel (ano, ne)
     * @param positionTop vrchní souřadníce
     * @param positionLeft levá souřadníce
     */
    public Area(String name, String description, boolean canSpawnEnemy, Double positionTop, Double positionLeft)
    {
        this.id = ++GameWorld.lastAreaId;

        this.name = name;
        this.description = description;
        this.canSpawnEnemy = canSpawnEnemy;
        this.positionTop = positionTop;
        this.positionLeft = positionLeft;

        this.exits = new HashMap<>();
        this.items = new TreeMap<>();
    }

    /**
     * Vrací ID lokace
     * 
     * @return Vrací Id lokace 
     */
    public int getId()
    {
        return id;
    }

    /**
     * Metoda vrací název lokace, který byl zadán při vytváření instance jako
     * parametr konstruktoru. Jedná se o jednoznačný identifikátor lokace
     * <i>(ve hře nemůže existovat více lokací se stejným názvem)</i>. Aby
     * hra správně fungovala, název lokace nesmí obsahovat mezery, v případě
     * potřeby můžete více slov oddělit pomlčkami, použít camel-case apod.
     *
     * @return název lokace
     */
    public String getName()
    {
        return name;
    }

    /**
     * Metoda vrací popis lokace.
     *
     * @return popis lokace
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Metoda vrací kompletní informace o lokaci. Výsledek volání obsahuje:
     * <ul>
     *     <li>název lokace</li>
     *     <li>popis lokace</li>
     *     <li>seznam sousedních lokací, do kterých lze odejít</li>
     *     <li>seznam předmětů v lokaci</li>
     * </ul>
     *
     * @return kompletní informace o lokaci
     */
    public String getFullDescription()
    {
        String exitNames = "Východy:";
        for (String exitName : exits.keySet()) {
            exitNames += " " + exitName + ",";
        }

        String itemNames = "Předměty:";
        for (String itemName : items.keySet()) {
            itemNames += " " + itemName + ",";
        }

        return "Jsi v lokaci '" + name + "'.\n"
        + description + "\n\n"
        + exitNames + "\n"
        + itemNames;
    }

    public Collection<String> getExitsForObserver()
    {
        return new HashSet<>(exits.keySet());
    }

    /**
     * Metoda přidá další východ z této lokace do lokace předané v parametru.
     * <p>
     * Vzhledem k tomu, že pro uložení sousedních lokací se používá {@linkplain Set},
     * může být přidán pouze jeden východ do každé lokace <i>(tzn. nelze mít dvoje
     * 'dveře' do stejné sousední lokace)</i>. Druhé volání metody se stejnou
     * lokací proto nebude mít žádný efekt.
     * <p>
     * Lze zadat též cestu do sebe sama.
     *
     * @param exit lokace, do které bude vytvořen východ z aktuální lokace
     */
    public void addExit(Area exit)
    {
        exits.put(exit.getName(), exit);
    }

    /**
     * Metoda podobná stejnojmenné metodě. Rozdíl je v tom, že se tato metoda používá
     * v herní mapě, kde druhý parametr charakterituje směr (Sever, Jih, Východ, Západ)
     * 
     * @param exit lokace, do které bude vytvořen východ z aktuální lokace
     */
    public void addExit(Area exit, String direction)
    {
        exits.put(direction, exit);
    }

    /**
     * Metoda vrátí kolekci všech sousedních lokací.
     *
     * @return kolekce sousedních lokací
     */
    public Collection<Area> getExits()
    {
        return new HashSet<>(exits.values());
    }

    /**
     * Metoda zkontroluje, zda lokace sousedí s lokací s daným názvem.
     *
     * @param exitName název lokace
     * @return {@code true}, pokud lokace sousedí s lokací s daným názvem; jinak {@code false}
     */
    public boolean hasExit(String exitName)
    {
        return exits.containsKey(exitName);
    }

    /**
     * Metoda vyhledá sousední lokaci s daným názvem a vrátí na ní odkaz.
     *
     * @param exitName název lokace
     * @return lokace s daným názvem; {@code null}, pokud lokace s takto pojmenovanou lokací nesousedí
     */
    public Area getExit(String exitName)
    {
        return exits.get(exitName);
    }

    /**
     * Metoda přidá předmět <i>(objekt třídy {@link Item})</i> do lokace.
     *
     * @param item předmět, který bude do lokace přidán
     */
    public void addItem(Item item)
    {
        items.put(item.getName(), item);
        notifyObservers();
    }

    /**
     * Metoda vrátí kolekci všech předmětů v lokaci.
     *
     * @return kolekce předmětů v lokaci
     */
    public Collection<Item> getItems()
    {
        return new HashSet<>(items.values());
    }

    /**
     * Metoda vrátí true nebo false, záleží na tom, zda je místnosti nějaký předmět.
     *
     * @return {@code true} pokud místnost má předměty, jonak {@code false}
     */
    public boolean hasItems()
    {
        return !items.isEmpty();
    }

    /**
     * Metoda zkontroluje, zda lokace obsahuje předmět s daným názvem.
     *
     * @param itemName název předmětu
     * @return {@code true}, pokud lokace obsahuje předmět s daným názvem; jinak {@code false}
     */
    public boolean containsItem(String itemName)
    {
        return items.containsKey(itemName);
    }

    /**
     * Metoda vyhledá v lokaci předmět s daným názvem a vrátí na něj odkaz.
     *
     * @param itemName název předmětu
     * @return předmět s daným názvem; {@code null}, pokud v lokaci není
     */
    public Item getItem(String itemName)
    {
        return items.get(itemName);
    }

    /**
     * Metoda vyhledá v lokaci předmět s daným názvem, odstraní ho z lokace a vrátí na něj odkaz.
     *
     * @param itemName název předmětu
     */
    public void removeItem(String itemName)
    {
        items.remove(itemName);
        notifyObservers();
    }

    /**
     * Metoda vrací informaci, jestli jde v lokaci vytvořit nepřítel.
     *
     * @return {@code true}, pokud jde spawnout nepřítel; jinak {@code false}
     */
    public boolean canSpawnEnemy()
    {
        return this.canSpawnEnemy;
    }

    /**
     * Metoda vrací informaci, je v lokaci nepřítel.
     *
     * @return {@code true}, pokud nepřítel v lokaci je; jinak {@code false}
     */
    public boolean hasEnemy()
    {
        return this.getEnemy() != null;
    }

    /**
     * Metoda vrátí nepřítele v lokaci, pokud tam nějaký je.
     *
     * @return nepřítel v dané lokaci; {@code null}, pokud žádný nepřítel v lokaci není
     */
    public Enemy getEnemy()
    {
        return this.enemy;
    }

    /**
     * Metoda nastaví nepřítele <i>(objekt třídy {@link Enemy})</i> do lokace.
     *
     * @param enemy nepřítel, který bude do lokace nastaven
     */
    public void setEnemy(Enemy enemy)
    {
        this.enemy = enemy;
        notifyObservers();
    }

    /**
     * Metoda nastaví bosse <i>(objekt třídy {@link Enemy})</i> do lokace.
     *
     * @param enemy boss, který bude do lokace nastaven
     */
    public void setBoss(Enemy enemy)
    {
        enemy.setIsBoss(true);

        this.enemy = enemy;
    }

    /**
     * Metoda náhodně, podle zvolené pravděpodobnosti, rozhodne, jestli se má vytvořit nepřítel.
     *
     * @return {@code true}, pokud se má nepřítel spawnout; jinak {@code false}
     */
    public boolean isSpawnedNewEnemy() {
        boolean isSpawned = false;

        if (ThreadLocalRandom.current().nextInt(1, Enemy.SPAWN_RATIO + 1) == Enemy.SPAWN_RATIO) {
            isSpawned = true;
        }

        return isSpawned;
    }

    /**
     * Metoda porovnává dvě lokace <i>(objekty)</i>. Lokace jsou shodné,
     * pokud mají stejný název <i>(atribut {@link #name})</i>. Tato metoda
     * je důležitá pro správné fungování seznamu východů do sousedních
     * lokací.
     * <p>
     * Podrobnější popis metody najdete v dokumentaci třídy {@linkplain Object}.
     *
     * @param o objekt, který bude porovnán s aktuálním
     * @return {@code true}, pokud mají obě lokace stejný název; jinak {@code false}
     *
     * @see Object#equals(Object)
     */
    @Override
    public boolean equals(final Object o)
    {
        if (o == this) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (o instanceof Area) {
            Area area = (Area) o;

            return name.equals(area.getName());
        }

        return false;
    }

    /**
     * Metoda vrací číselný identifikátor instance, který se používá
     * pro optimalizaci ukládání v dynamických datových strukturách
     * <i>(např.&nbsp;{@linkplain HashSet})</i>. Při překrytí metody
     * {@link #equals(Object) equals} je vždy nutné překrýt i tuto
     * metodu.
     * <p>
     * Podrobnější popis pravidel pro implementaci metody najdete
     * v dokumentaci třídy {@linkplain Object}.
     *
     * @return číselný identifikátor instance
     *
     * @see Object#hashCode()
     */
    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    /**
     * Metoda porovná lokaci s druhou lokací předané v parametru
     * abecedně dle jejich názvů a vrátí kladné číslo, nulu, nebo záporné
     * číslo v závislosti na tom, zda je název této lokace větší,
     * stejný, nebo menší než název druhé lokace.
     * <p>
     * Metoda se používá pro řazení sousedních lokací v atributu {@link #exits}.
     *
     * @param area lokace, jejíž název bude porovnán s názvem této lokace
     * @return kladné číslo, nula, nebo záporné číslo v závislosti na porovnání názvů lokací
     */
    @Override
    public int compareTo(Area area)
    {
        return name.compareTo(area.getName());
    }

    /**
     * Metoda vrací vrhcní pozici
     *
     * @return double číslo vrhní pozice
     */
    public double getPositionTop() {
        return positionTop;
    }

    /**
     * Metoda vrací levou pozici
     *
     * @return double číslo levé pozice
     */
    public double getPositionLeft() {
        return positionLeft;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
