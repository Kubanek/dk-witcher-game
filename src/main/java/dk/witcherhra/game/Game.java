package dk.witcherhra.game;

import dk.witcherhra.game.action.*;
import dk.witcherhra.util.Observer;
import dk.witcherhra.util.SubjectOfChange;

import java.util.*;

/**
 * Hlavní třída hry. Vytváří a uchovává odkaz na instanci třídy {@link GameWorld}
 * a také vytváří seznam platných příkazů a instance tříd provádějících jednotlivé
 * příkazy. Vytváří a uchovává odkaz na třídy {@link Tasks} [třída se stará o úkoly],
 * {@link Witcher} [třída se stará o hráčovu postavu], {@link Witcher} 
 * [třída se stará o hráčovu postavu]
 * <p>
 * Během hry třída vyhodnocuje jednotlivé příkazy zadané uživatelem.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class Game implements SubjectOfChange
{

    public static final String FILE_IS_MISSING = "Chybí soubor";

    private boolean gameOver;

    private GameWorld world;
    private Witcher witcher;
    private Tasks tasks;
    private Inventory inventory;

    private Set<IAction> actions;
    private final Set<Observer> observers = new HashSet<>();
    /**
     * Konstruktor třídy, vytvoří hru a množinu platných příkazů. Hra po
     * vytvoření již běží a je připravená zpracovávat herní příkazy.
     */
    public Game()
    {
        world = new GameWorld();
        tasks = new Tasks();

        witcher = new Witcher();
        inventory = new Inventory();

        actions = new HashSet<>();
        actions.add(new ActionHelp(this));
        actions.add(new ActionTerminate(this));
        actions.add(new ActionMove(this));
        actions.add(new ActionInvestigateLocation(this));
        actions.add(new ActionCharacter(this));
        actions.add(new ActionLookAround(this));
        actions.add(new ActionPick(this));
        actions.add(new ActionPickAll(this));
        actions.add(new ActionDropItem(this));
        actions.add(new ActionDropAll(this));
        actions.add(new ActionFallBack(this));
        actions.add(new ActionAttack(this));
        actions.add(new ActionDrinkPotion(this));
        actions.add(new ActionUpradgeItem(this));
        actions.add(new ActionSleep(this));
        actions.add(new ActionHaveFun(this));
        actions.add(new ActionEat(this));
    }

    /**
     * Metoda vrací informaci, zda hra již skončila <i>(je jedno, jestli
     * výhrou, prohrou nebo příkazem 'konec')</i>.
     *
     * @return {@code true}, pokud hra již skončila; jinak {@code false}
     */
    public boolean isGameOver()
    {
        return gameOver;
    }

    /**
     * Metoda nastaví příznak indikující, zda hra skončila. Metodu
     * využívá třída {@link ActionTerminate}, mohou ji ale použít
     * i další implementace rozhraní {@link IAction}.
     *
     * @param gameOver příznak indikující, zda hra již skončila
     */
    public void setGameOver(boolean gameOver)
    {
        this.gameOver = gameOver;
        this.notifyObservers();
    }

    /**
     * Metoda vrací odkaz na mapu prostorů herního světa.
     *
     * @return mapa prostorů herního světa
     */
    public GameWorld getWorld()
    {
        return world;
    }
    
    /**
     * Metoda vrátí kolekci všech předmětů v lokaci.
     *
     * @return kolekce předmětů v lokaci
     */
    public HashSet<IAction> getActions()
    {
        return new HashSet<>(actions);
    }

    /**
     * Metoda vrací odkaz na zaklínače (hráče).
     *
     * @return Zaklínač
     */
    public Witcher getWitcher()
    {
        return witcher;
    }

    /**
     * Metoda vrací odkaz na úkoly.
     *
     * @return Úkoly z prostoru herního světa
     */
    public Tasks getTasks()
    {
        return tasks;
    }

    /**
     * Metoda vrací odkaz na hráčův inventář.
     *
     * @return hráčův inventář
     */
    public Inventory getInventory()
    {
        return inventory;
    }

    /**
     * Metoda zpracuje herní příkaz <i>(jeden řádek textu zadaný na konzoli)</i>.
     * Řetězec uvedený jako parametr se rozdělí na slova. První slovo je považováno
     * za název příkazu, další slova za jeho parametry. 
     * <p>
     * Metoda nejprve ověří, zda první slovo odpovídá hernímu příkazu <i>(např.
     * 'napoveda', 'konec', 'jdi' apod.)</i>. Pokud ano, spustí obsluhu tohoto
     * příkazu a zbývající slova předá jako parametry.  Pokud je hra právě zapnuta, metoda hráčí dovolí
     * používat pouze prikazy Konec, Napoveda a Jdi Hostinec.
     *
     * @param line text, který hráč zadal na konzoli jako příkaz pro hru
     * @return výsledek zpracování <i>(informace pro hráče, které se vypíšou na konzoli)</i>
     */
    public String processAction(String line)
    {
        String[] words = line.split("[ \t]+");

        String actionName = words[0];
        String[] actionParameters = new String[words.length - 1];

        for (int i = 0; i < actionParameters.length; i++) {
            actionParameters[i] = words[i + 1];
        }

        if (!tasks.areTaskAssigned()) {
            if (line.equals("Konec") || line.equals("Napoveda") || line.equals("Jdi Hostinec")) {
                for (IAction executor : actions) {
                    if (executor.getName().equals(actionName)) {
                        return executor.execute(actionParameters);
                    }
                } 
            }

            return "Pokud chcete hru opravdu začít, použijte příkaz 'Jdi Hostinec'.";
        }

        String result = "Tento příkaz není definovaný.";

        for (IAction executor : actions) {
            if (executor.getName().equals(actionName)) {
                result = executor.execute(actionParameters);
            }
        }

        if (world.getGameState() != GameState.PLAYING) {
            setGameOver(true);
        }

        return result;
    }

    /**
     * Metoda vrací úvodní text pro hráče, který se vypíše na konzoli ihned po
     * zahájení hry.
     *
     * @return úvodní text
     */
    public String getPrologue()
    {
        return "Vítejte!\n"
        + "Toto je příběh o Geraltovi, známém bělovlasém zaklínači. Ač je tvé jméno více než známé, pověsti o zaklínačích jsou známější.\n"
        + "Vaším cílem bude splnit všechny tři úkoly, které získáš v Hostinci. Jakmile tyto úkoly splníte, místní obyvatelé vám budou věřit a budete se moc ucházet o závěřečný úkol na hradě.\n"
        + "Pokud chcete začít tento příběh zadejte příkaz 'Jdi Hostinec'. Hodně štěstí!\n"
        + "Nevíte-li, jaké všechny příkazy můžete používat, zadejte příkaz 'Napoveda'.";
    }

    /**
     * Metoda vrací závěrečný text pro hráče, který se vypíše na konzoli po ukončení
     * hry. Metoda se zavolá pro všechna možná ukončení hry <i>(hráč vyhrál, hráč
     * prohrál, hráč ukončil hru příkazem 'konec')</i>. Tyto stavy je vhodné
     * v metodě rozlišit.
     *
     * @return závěrečný text
     */
    public String getEpilogue()
    {
        String epilogue = "Díky, že sis zahrál(a).";

        GameState gameState = world.getGameState();

        if (gameState == GameState.WON) {
            epilogue = "Vyhrál(a) jsi.\n\n" + epilogue;
        }
        
        return epilogue;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
