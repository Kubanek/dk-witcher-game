package dk.witcherhra.game;

import java.util.*;

/**
 * Třída slouží k nadefinování atributů různých bossů, následné vygenerování bossů a uložení do HashMap konstanty
 * a v případě potřeby k následnému vybrání chtěného bosse z konstanty.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 *
 * DOPLNĚNY KOMENTÁŘE
 *
 */
public class Bosses
{
    public static String BANDIT_BOSS = "Vůdce banditů (boss)";
    public static int BANDIT_BOSS_HP = 150;
    public static int BANDIT_BOSS_LOWER_DAMAGE = 7;
    public static int BANDIT_BOSS_UPPER_DAMAGE = 15;
    public static int BANDIT_BOSS_DODGE_NUMBER = 6;
    
    public static String TROLL_BOSS = "Troll (boss)";
    public static int TROLL_BOSS_HP = 200;
    public static int TROLL_BOSS_LOWER_DAMAGE = 8;
    public static int TROLL_BOSS_UPPER_DAMAGE = 18;
    public static int TROLL_BOSS_DODGE_NUMBER = 100;
    
    public static String KIDNAPPER_BOSS = "Únosce (boss)";
    public static int KIDNAPPER_BOSS_HP = 120;
    public static int KIDNAPPER_BOSS_LOWER_DAMAGE = 12;
    public static int KIDNAPPER_BOSS_UPPER_DAMAGE = 16;
    public static int KIDNAPPER_BOSS_DODGE_NUMBER = 3;

    public static String DRAGON_BOSS = "Drak (boss)";
    public static int DRAGON_BOSS_HP = 420;
    public static int DRAGON_BOSS_LOWER_DAMAGE = 10;
    public static int DRAGON_BOSS_UPPER_DAMAGE = 25;
    public static int DRAGON_BOSS_DODGE_NUMBER = 50;
    
    private static final Map<String, Enemy> bosses = new HashMap<String, Enemy>() {{
                put(BANDIT_BOSS, new Enemy(BANDIT_BOSS, BANDIT_BOSS_HP, BANDIT_BOSS_LOWER_DAMAGE, BANDIT_BOSS_UPPER_DAMAGE, BANDIT_BOSS_DODGE_NUMBER));
                put(TROLL_BOSS, new Enemy(TROLL_BOSS, TROLL_BOSS_HP, TROLL_BOSS_LOWER_DAMAGE, TROLL_BOSS_UPPER_DAMAGE, TROLL_BOSS_DODGE_NUMBER));
                put(KIDNAPPER_BOSS, new Enemy(KIDNAPPER_BOSS, KIDNAPPER_BOSS_HP, KIDNAPPER_BOSS_LOWER_DAMAGE, KIDNAPPER_BOSS_UPPER_DAMAGE, KIDNAPPER_BOSS_DODGE_NUMBER));
                put(DRAGON_BOSS, new Enemy(DRAGON_BOSS, DRAGON_BOSS_HP, DRAGON_BOSS_LOWER_DAMAGE, DRAGON_BOSS_UPPER_DAMAGE, DRAGON_BOSS_DODGE_NUMBER));
            }};

    /**
     * Metoda vrátí instanci nepřítele(bosse) podle jeho názvu z konstanty "bosses".
     *
     * @param name název bosse
     * @return nepřítel(boss); {@code null}, pokud se vyhodí vyjímka (nemělo by se stát)
     */
    public static Enemy getBoss(String name) {
        try {
            return bosses.get(name).clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
