package dk.witcherhra.game;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Rozhraní definující metody nutné pro implementaci charakteru.
 * Používá se pro nepřítele {@link Enemy} a hráčovu postavu {@link Witcher}.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 */
public interface ICharacters
{
    /**
     * Metoda nastaví postavě životy.
     *
     * @param hpBar životy
     */
    void setHpBar(int hpBar);

    /**
     * Metoda vrátí životy postavy.
     *
     * @return životy postavy
     */
    int getHpBar();
    
    /**
     * Metoda vrátí spodní hranici poškození postavy.
     *
     * @return spodní hranice poškození postavy
     */
    int getLowerDamage();
    
    /**
     * Metoda vrátí horní hranici poškození postavy.
     *
     * @return horní hranice poškození postavy
     */
    int getUpperDamage();
    
    /**
     * Metoda vrátí kolik poškození postava udělí/udělila.
     *
     * @return udělené poškození
     */
    int damageDealed();
    
    /**
     * Metoda vrátí, jestli bude/byl útok postavou odvrácen.
     *
     * @return {@code true}, pokud se postava vyhne útoku; pokud ne tak {@code false}
     */
    boolean isAttackDodged();
    
    /**
     * Metoda vrátí popis postavy.
     *
     * @return popis postavy
     */
    String getDescription();
}
