package dk.witcherhra.game;

import dk.witcherhra.util.Observer;
import dk.witcherhra.util.SubjectOfChange;

import java.util.*;

/**
 * Třída implementující inventář. Ve třdě se nachází všechny metody spojené
 * s inventářem a penežmi. Ve třídě se nachází také statické hodnoty spojené
 * s inventářem.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class Inventory implements SubjectOfChange {

    public static String EQUIPMENT_NAME = "Vylepšeno:";
    public static String INVENTORY_NAME = "Inventář:";
    public static String INVENTORY_IS_FULL = "Inventář je plný.";
    public static String INVENTORY_IS_EMPTY = "V inventáři se nenachází žádný předmět.";
    public static String ALL_ITEMS_WERE_REMOVE_FROM_INVENTORY = "Všechny věci se z inventářě přemístily do této lokace.";

    public static String SWORD = "Mec";
    public static int UPRADGED_LOWER_DAMAGE = 15;
    public static int UPRADGED_UPPER_DAMAGE = 25;
    public static int SWORD_COST = 200;

    public static String ARMOR = "Brneni";
    public static int UPRADGED_MAX_HP = 120;
    public static int UPRADGED_DODGE_NUMBER = 3;
    public static int ARMOR_COST = 180;

    public static String POTION_SAC = "Lektvarovy_vak";
    public static int UPRADGED_MAXIMUM_OF_POTIONS = 2;
    public static int POTION_SAC_COST = 150;

    public static int HEALING_POTION_REGENERATE = 50;
    public static int AFTER_BOSS_REGENERATE = 20;

    int inventoryCapacity;
    int money;
    int maximumOfHealingPotions;
    int healingPotions;

    boolean isUpradgedDamage;
    boolean isUpradgedArmor;
    boolean isUpradgedPotionSizeInventory;

    private final Map<String, Item> inventoryItems;
    private final Set<Observer> observers = new HashSet<>();

    /**
     * Konstruktor třídy, vytvoří inventář a nastaví základní herní parametry
     * (kapacitu, peníze, léčebné lektvary a max. počet léč. lektvarů) a samotnou
     * 'logiku' inventáře
     */
    public Inventory() {
        this.inventoryCapacity = 3;
        this.money = 20;
        this.maximumOfHealingPotions = 1;
        this.healingPotions = 1;
        this.inventoryItems = new TreeMap<>();
    }

    /**
     * Metoda vrací informaci o tom, zda je inventář plný.
     *
     * @return {@code true}, pokud je inventář plný; jinak {@code false}
     */
    public boolean isInventoryFull() {
        boolean isInventoryFull = true;

        if (inventoryItems.size() < inventoryCapacity) {
            isInventoryFull = false;
        }

        return isInventoryFull;
    }

    /**
     * Metoda vrací informaci o tom, zda je inventář prázdný.
     *
     * @return {@code true}, pokud je inventář prázdný; jinak {@code false}
     */
    public boolean isInventoryEmpty() {
        return inventoryItems.isEmpty();
    }

    /**
     * Metoda vrátí kolekci všech předmětů z inventáře.
     *
     * @return kolekce předmětů z inventáře
     */
    public Collection<Item> getItemsFromInventory() {
        return new HashSet<>(inventoryItems.values());
    }

    /**
     * Metoda zkontroluje, zda inventář obsahuje předmět s daným názvem.
     *
     * @param itemName název předmětu
     * @return {@code true}, pokud inventář obsahuje předmět s daným názvem; jinak {@code false}
     */
    public boolean containsItem(String itemName) {
        return inventoryItems.containsKey(itemName);
    }

    /**
     * Metoda vyhledá v inventáři předmět s daným názvem a vrátí na něj odkaz.
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param itemName název předmětu
     * @return předmět s daným názvem; {@code null}, pokud v inventáři není
     */
    public Item getItem(String itemName) {
        return inventoryItems.get(itemName);
    }

    /**
     * Metoda přidá předmět do invetáře. Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param item (předmět), který se uloží do invetnáře
     */
    public void addItemToInventory(Item item) {
        inventoryItems.put(item.getName(), item);
        notifyObservers();
    }

    /**
     * Metoda vyhledá v inventáři předmět s daným názvem a odstraní jej z inventář
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param itemName název předmětu
     */
    public void removeItemFromInventory(String itemName) {
        inventoryItems.remove(itemName);
        notifyObservers();
    }

    /**
     * Metoda vrátí počet peněz v inventáři
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return money (integer); počet peněz co hráč vlastní
     */
    public int getMoney() {
        return money;
    }

    /**
     * Metoda vrátí počet peněz v inventáři
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return money (string); počet peněz co hráč vlastní
     */
    public String getTextedMoney() {
        return "Počet zlotých: " + getMoney();
    }

    /**
     * Metoda nastavuje hodnotu peněz v inventáři. Metoda se používá k odčítání financí
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param moneyChange   hodnota penež, která se odečítá
     * @param isSubtraction {@code true}, v kodu se používá pouze hodnota true, slouží k rozdělení
     *                      přičítání a odčítání peněz
     */
    public void setMoney(int moneyChange, boolean isSubtraction) {
        int money = this.money - moneyChange;

        if (isSubtraction) {
            this.money = money;
        }
        notifyObservers();
    }

    /**
     * Metoda nastavuje hodnotu peněz v inventáři. Metoda se používá k přičítání financí
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param moneyChange hodnota penež, která se přičítá
     */
    public void setMoney(int moneyChange) {
        int money = this.money + moneyChange;

        this.money = money;
        notifyObservers();
    }

    /**
     * Metoda vrací maximální počet lekvatů
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return maximální počet léčebných nápojů (int)
     */
    public int getMaximumOfHealingPotions() {
        return maximumOfHealingPotions;
    }

    /**
     * Metoda nastavuje maximální počet lektvarů
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param maximumOfHealingPotions počet léčebných lekvarů, které nově definuje maximum lekvarů,
     *                                které může hráč mít
     */
    public void setMaximumOfHealingPotions(int maximumOfHealingPotions) {
        this.maximumOfHealingPotions = maximumOfHealingPotions;
        notifyObservers();
    }

    /**
     * Metoda vrací text s počtem lekvatů
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return text s počtem lekvatů (string)
     */
    public String getTextedCurrentNumberOfPotions() {
        return "Počet léčebných elixírů: " + getNumberOfHealingPotions() + " z " + getMaximumOfHealingPotions();
    }

    /**
     * Metoda vrací aktuální počet lekvarů
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return aktuální počet léčebných nápojů (int)
     */
    public int getNumberOfHealingPotions() {
        return healingPotions;
    }

    /**
     * Metoda nastavuje aktuální počet lektvarů
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param numberOfhealingPotions nastavuje počet léčebných lekvarů, které hráč aktuálně vlastní
     */
    public void setNumberOfHealingPotions(int numberOfhealingPotions) {
        this.healingPotions = numberOfhealingPotions;
        notifyObservers();
    }

    /**
     * Metoda vrací zda byl vylepšený meč
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return {@code true}, pokud byl vylepšený meč; pokud ne tak {@code false}
     */
    public boolean isUpradgedDamage() {
        return isUpradgedDamage;
    }

    /**
     * Metoda nastavuje, zda byl meč vylepšen.
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param isUpradgedDamage byl meč vylepšen
     */
    public void setIsUpradgedDamage(boolean isUpradgedDamage) {
        this.isUpradgedDamage = isUpradgedDamage;
    }

    /**
     * Metoda vrací zda bylo brnění vylepšeno.
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return {@code true}, pokud bylo brnění vylepšeno; pokud ne tak {@code false}
     */
    public boolean isUpradgedArmor() {
        return isUpradgedArmor;
    }

    /**
     * Metoda vrací zda je něco vylepšeno
     * Metodu využívají metody v rozhraní {@link dk.witcherhra.gui.PlayerPanel}.
     *
     * @return {@code true}, pokud ano; pokud ne tak {@code false}
     */
    public boolean isSomethingUpgraded() {
        if (isUpradgedDamage() || isUpradgedArmor() || isUpradgedPotionSizeInventory()) {
            return true;
        }

        return false;
    }

    /**
     * Metoda nastavuje, zda bylo brnění vylepšeno.
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param isUpradgedArmor byl meč vylepšen
     */
    public void setIsUpradgedArmor(boolean isUpradgedArmor) {
        this.isUpradgedArmor = isUpradgedArmor;
    }

    /**
     * Metoda vrací, zda byl vylepšen lektvarový vak, aby mohl mít více lektvarů.
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @return {@code true}, pokud byl lektvarový vak pro lektvary vylepšen; pokud ne tak {@code false}
     */
    public boolean isUpradgedPotionSizeInventory() {
        return isUpradgedPotionSizeInventory;
    }

    /**
     * Metoda nastavuje, zda byl lektvarový vak na lektvary vylepšen.
     * Metodu využívají metody v rozhraní {@link IAction}.
     *
     * @param isUpradgedPotionSizeInventory byl lektvarový vak vylepšen
     */
    public void setIsUpradgedPotionSizeInventory(boolean isUpradgedPotionSizeInventory) {
        this.isUpradgedPotionSizeInventory = isUpradgedPotionSizeInventory;
    }

    @Override
    public void registerObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }
}
