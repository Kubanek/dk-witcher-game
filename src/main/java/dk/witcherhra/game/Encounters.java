package dk.witcherhra.game;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Třída slouží k nadefinování atributů různých nepřátel, následné vygenerování nepřátel a uložení do HashMap konstanty
 * a v případě potřeby k následnému vybrání chtěného nepřítele z konstanty, nebo náhodného nepřítele.
 *
 * @author Daniel Kubánek
 * @version LS-2021, 2021-06-10
 */
public class Encounters
{

    public static String BANDIT= "Bandita";
    public static String ARMORER = "Zbrojíř";

    public static String WOLF = "Vlk";
    public static String DROWNER = "Topivec";
    public static String NECCER = "Nekker";
    public static String KIKIMORE = "Kikimora";

    public static String BRUXA = "Bruxa";

    public static int WAMPIRE_HP = 80;
    public static int PERSON_HP = 50;
    public static int BEAST_HP = 30;

    public static int PERSON_LOWER_DMG = 5;
    public static int PERSON_UPPER_DMG = 8;

    public static int BEAST_LOWER_DMG = 6;
    public static int BEAST_UPPER_DMG = 10;

    public static int WAMPIRE_LOWER_DMG = 8;
    public static int WAMPIRE_UPPER_DMG = 13;

    public static int PERSON_DODGE_NUMBER = 8;
    public static int BEAST_DODGE_NUMBER = 5;
    public static int WAMPIRE_DODGE_NUMBER = 5;

    private static final Map<String, Enemy> enemies = new HashMap<String, Enemy>() {{
                put(BANDIT, new Enemy(BANDIT, PERSON_HP, PERSON_LOWER_DMG, PERSON_UPPER_DMG, PERSON_DODGE_NUMBER));
                put(ARMORER, new Enemy(ARMORER, PERSON_HP, PERSON_LOWER_DMG, PERSON_UPPER_DMG, PERSON_DODGE_NUMBER));
                put(WOLF, new Enemy(WOLF, BEAST_HP, BEAST_LOWER_DMG, BEAST_UPPER_DMG, BEAST_DODGE_NUMBER));
                put(DROWNER, new Enemy(DROWNER, BEAST_HP, BEAST_LOWER_DMG, BEAST_UPPER_DMG, BEAST_DODGE_NUMBER));
                put(NECCER, new Enemy(NECCER, BEAST_HP, BEAST_LOWER_DMG, BEAST_UPPER_DMG, BEAST_DODGE_NUMBER));
                put(KIKIMORE, new Enemy(KIKIMORE, BEAST_HP, BEAST_LOWER_DMG, BEAST_UPPER_DMG, BEAST_DODGE_NUMBER));
                put(BRUXA, new Enemy(BRUXA, WAMPIRE_HP, WAMPIRE_LOWER_DMG, WAMPIRE_UPPER_DMG, WAMPIRE_DODGE_NUMBER));
            }};

    /**
     * Metoda vrátí nepřítele podle jeho názvu.
     *
     * @param name název nepřítele
     * @return vyžádaný nepřítel; {@code null}, pokud se vyhodí vyjímka (nemělo by se stát)
     */
    public static Enemy getEnemy(String name) {
        try {
            return enemies.get(name).clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /**
     * Metoda vrátí náhodného nepřítele z kolekce všech možných nepřátel.
     *
     * @return náhodný nepřítel; {@code null}, pokud se vyhodí vyjímka (nemělo by se stát)
     */
    public static Enemy getRandomEnemy() {
        try {
            int size = Encounters.enemies.size();
            int random = ThreadLocalRandom.current().nextInt(size);
            int i = 0;

            for(Enemy enemy : Encounters.enemies.values())
            {
                if (i == random) {
                    return enemy.clone();
                }

                i++;
            }

            return enemies.get(BANDIT).clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
