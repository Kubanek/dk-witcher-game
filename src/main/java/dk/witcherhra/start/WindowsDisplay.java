package dk.witcherhra.start;

public class WindowsDisplay {

    public static int WINDOW_WIDTH_DISPLAY = 1540;
    public static int WINDOW_HEIGHT_DISPLAY = 1000;

    public static int GAME_PLAN_WIDTH_DISPLAY = 1300;
    public static int GAME_PLAN_HEIGHT_DISPLAY = 480;

    public static int BOXES_WIDTH = 350;
    public static int BOXES_HEIGHT = 800;

    /** {@link dk.witcherhra.gui.GamePlan}*/
    public static int HORSE_WIDTH_DISPLAY = 60;
    public static int HORSE_HEIGHT_DISPLAY = 45;

    /** {@link dk.witcherhra.gui.PlayerPanel}*/
    public static int WITCHER_STUFF_DISPLAY = 120;
    public static int ITEMS_IN_INVENTORY_DISPLAY = 120;
    public static int INVENTORY_HEIGHT = 320;
    public static int INVENTORY_WIDTH = 500;

    public static int EQUIPMENT_DISPLAY = 50;
    /** {@link dk.witcherhra.gui.PositionPanel}*/
    public static int CURRENT_AREA_HEIGHT = 150;
    public static int CURRENT_AREA_WIDTH = 150;

    /** {@link Start}*/
    public static int CONSOLE_WIDTH = WINDOW_WIDTH_DISPLAY - (BOXES_WIDTH * 2);

}
