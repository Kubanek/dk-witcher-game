package dk.witcherhra.start;

import dk.witcherhra.game.action.ActionHelp;
import dk.witcherhra.gui.PositionPanel;
import dk.witcherhra.gui.GamePlan;
import dk.witcherhra.gui.PlayerPanel;
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import dk.witcherhra.game.*;
import dk.witcherhra.scenarioTester.Runner;
import dk.witcherhra.textUi.TextUI;

import java.io.InputStream;
import java.net.URL;

/**
 * Spouštěcí třída aplikace.
 *
 * @author Daniel Kubánek
 * @version ZS-2022, 2021-05-10
 */
public class Start extends Application
{

    public static String GAME_NAME = "Witcher - hra";
    public static String FILL_COMMAND = "Zadejte příkaz: ";
    public static String FILLED_COMMAND = "Zadaný příkaz: ";

    public static String NEW_GAME = "Nová Hra";
    public static String END_GAME = "Konec";

    public static String MENU_FILE = "Soubor";
    public static String HOW_TO_PLAY = "Herní manuál";
    public static String MENU_HELP = "Nápověda";

    public static String HELP_HTML = "Help.html";

    public static TextArea console = new TextArea();
    public static TextField consoleTextField = new TextField();

    private static Game game = new Game();

    /**
     * Spouštěcí metoda aplikace. Vyhodnotí parametry, se kterými byla aplikace
     * spuštěna, a na základě nich rozhodne, jakou operaci provede <i>(hra, hra -textová verze, výpis
     * testovacích scénářů, spuštění testovacích scénářů)</i>.
     * <p>
     * Pokud byla aplikace spuštěna s nepodporovanými parametry, vypíše nápovědu
     * a skončí.
     *
     * @param args parametry aplikace z příkazové řádky
     */
    public static void main(String[] args) {
        int numberOfArguments = args.length;

        if (numberOfArguments < 1) {
            launch(args);
        } else if (numberOfArguments == 1 && args[0].equalsIgnoreCase("TEXT")) {
            (createTextUi()).play();
        } else if (numberOfArguments == 1 && args[0].equalsIgnoreCase("SHOW_SCENARIOS")) {
            System.out.println((new Runner()).showAllScenarios());
        } else if (numberOfArguments == 1 && args[0].equalsIgnoreCase("RUN_SCENARIOS")) {
            System.out.println((new Runner()).runAllScenarios());
        } else {
            System.out.println("Hra byla spuštěna s nepodporovanými parametry!");

            System.out.println("\nAplikaci je možné spustit s následujícími parametry:");
            System.out.println("  <bez parametrů>     : Spustí hru");
            System.out.println("  SHOW_SCENARIOS      : Vypíše kroky testovacích scénářů");
            System.out.println("  RUN_SCENARIOS       : Provede testy testovacích scénářů");
        }

        System.exit(0);
    }

    @Override
    public void start(Stage primaryStage) {
        BorderPane borderPane = new BorderPane();
        GameWorld gameWorld = game.getWorld();

        GamePlan gamePlan = new GamePlan(gameWorld);
        PositionPanel exitsPanel = new PositionPanel(game);
        PlayerPanel inventoryPanel = new PlayerPanel(game);
        MenuBar menuBar = prepareMenuBar(primaryStage);

        prepareScene(primaryStage, borderPane, gamePlan, exitsPanel, inventoryPanel, menuBar);

        VBox centerVBox = new VBox();
        centerVBox.getChildren().addAll(createConsole(), createConsoleTextFieldHBox());
        borderPane.setCenter(centerVBox);

        consoleTextField.setOnAction(actionEvent -> {
            String command = consoleTextField.getText();
            console.appendText("\n\n---\n" + FILLED_COMMAND + command + "\n---\n" + "\n");
            consoleTextField.setText("");
            String answer = game.processAction(command);
            console.appendText(answer);

            if (game.isGameOver()) {
                consoleTextField.setEditable(false);
                console.appendText("\n" + game.getEpilogue());
            }
        });

        consoleTextField.requestFocus();
    }

    public static Image getImage(InputStream inputStream, String imagePng, int width, int height)
    {
        Image image = null;

        if (inputStream != null) {
            image = new Image(inputStream, width, height, false, false);
        } else {
            System.err.println(Game.FILE_IS_MISSING + " - " + imagePng);
            System.exit(-1);
        }

        return image;
    }

    //////////////////////////////////////////////////////// Utils

    public static TextArea getConsole()
    {
        return console;
    }

    public static TextField getConsoleTextField()
    {
        return consoleTextField;
    }

    //////////////////////////////////////////////////////// Private

    private static TextArea createConsole()
    {
        console.setText(game.getPrologue());
        console.setPrefSize(WindowsDisplay.CONSOLE_WIDTH, WindowsDisplay.BOXES_HEIGHT);
        console.setEditable(false);

        return console;
    }

    private static void prepareScene(
            Stage primaryStage,
            BorderPane borderPane,
            GamePlan gamePlan,
            PositionPanel positionPanel,
            PlayerPanel playerPanel,
            MenuBar menuBar
    ) {
        HBox world = gamePlan.getHBox();
        VBox positionPanelVBox = positionPanel.getPositionPanel();
        VBox playerInventoryVbox = playerPanel.getPlayerPanel();

        borderPane.setTop(world);
        borderPane.setRight(positionPanelVBox);
        borderPane.setLeft(playerInventoryVbox);

        VBox gameWithMenu = new VBox();
        gameWithMenu.getChildren().addAll(menuBar, borderPane);

        Scene scene = new Scene(gameWithMenu,  WindowsDisplay.WINDOW_WIDTH_DISPLAY, WindowsDisplay.WINDOW_HEIGHT_DISPLAY);
        primaryStage.setScene(scene);
        primaryStage.setTitle(GAME_NAME);
        primaryStage.show();
    }

    private static HBox createConsoleTextFieldHBox()
    {
        HBox hBoxTextField = new HBox();
        Label labelTextField = new Label(FILL_COMMAND);

        labelTextField.setFont(Font.font("Arial", FontWeight.BOLD,16));

        hBoxTextField.getChildren().addAll(labelTextField, consoleTextField);
        hBoxTextField.setAlignment(Pos.BOTTOM_CENTER);

        return hBoxTextField;
    }

    private MenuBar prepareMenuBar(Stage primaryStage) {
        MenuBar menuBar = new MenuBar();

        Menu fileMenu = new Menu(MENU_FILE);
        Menu helpMenu = new Menu(MENU_HELP);

        MenuItem newGame = new MenuItem(NEW_GAME);
        MenuItem howToPlay = new MenuItem(HOW_TO_PLAY);
        MenuItem endGame = new MenuItem(END_GAME);

        MenuItem help = new MenuItem(MENU_HELP);

        newGame.setAccelerator(KeyCombination.keyCombination("Ctrl+N"));

        fileMenu.getItems().addAll(newGame, new SeparatorMenuItem(), howToPlay, new SeparatorMenuItem(), endGame);
        helpMenu.getItems().addAll(help);

        newGame.setOnAction(actionEvent -> {
            primaryStage.close();

            game = new Game();
            consoleTextField.setEditable(true);

            start(new Stage());
        });

        howToPlay.setOnAction(actionEvent -> {
            Stage stage = new Stage();
            stage.setTitle(HOW_TO_PLAY);

            TextArea textArea = new TextArea();
            textArea.setText("Všechny herní příkazy:\n\n" + game.processAction(new ActionHelp(game).getName()));

            stage.setScene(new Scene(textArea));
            stage.show();
        });

        endGame.setOnAction(actionEvent -> {
            game.setGameOver(true);
            primaryStage.close();
        });

        help.setOnAction(actionEvent -> {
            Stage stage = new Stage();
            stage.setTitle(MENU_HELP);

            WebView webView = new WebView();
            URL url = Start.class.getResource("/resources/" + HELP_HTML);

            if (url != null) {
                webView.getEngine().load(url.toExternalForm());
            } else {
                System.err.println(Game.FILE_IS_MISSING + " - " + HELP_HTML);
                System.exit(-1);
            }

            stage.setScene(new Scene(webView, 600, 600));
            stage.show();
        });

        menuBar.getMenus().addAll(fileMenu, helpMenu);

        return menuBar;
    }

    private static TextUI createTextUi()
    {
        return new TextUI(game);
    }

}
