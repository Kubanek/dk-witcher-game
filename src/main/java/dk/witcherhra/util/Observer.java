package dk.witcherhra.util;

public interface Observer {

    void update();

}
