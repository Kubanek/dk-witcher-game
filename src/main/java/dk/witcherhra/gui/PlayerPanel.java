package dk.witcherhra.gui;

import dk.witcherhra.game.Game;
import dk.witcherhra.game.Inventory;
import dk.witcherhra.game.Item;
import dk.witcherhra.game.Witcher;
import dk.witcherhra.start.Start;
import dk.witcherhra.start.WindowsDisplay;
import dk.witcherhra.util.Observer;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.InputStream;
import java.util.Collection;

public class PlayerPanel implements Observer {

    private static final String POTION = "Potion";
    private static final String MONEY_PURSE = "MoneyPurse";

    private static final String POTION_PNG_ROOT_DIRECTORY = "/resources/" + POTION + ".png";
    private static final String MONEY_PURSE_ROOT_DIRECTORY = "/resources/" + MONEY_PURSE + ".png";

    private static final String SWORD_ROOT_DIRECTORY = "/resources/" + Inventory.SWORD + ".png";
    private static final String ARMOR_ROOT_DIRECTORY = "/resources/" + Inventory.ARMOR + ".png";
    private static final String POTION_SAC_ROOT_DIRECTORY = "/resources/" + Inventory.POTION_SAC + ".png";

    private final VBox vBox = new VBox();
    private final Witcher witcher;
    private final Inventory inventory;

    private final static Label witcherLabel = new Label(Witcher.WITCHER);
    private final static Label inventoryLabel = new Label(Inventory.INVENTORY_NAME);
    private final FlowPane witcherPanel = new FlowPane();
    private final ScrollPane inventoryPanel = new ScrollPane();
    private final HBox inventoryBox = new HBox();

    public PlayerPanel(Game game) {
        this.inventory = game.getInventory();
        this.witcher = game.getWitcher();

        witcherLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        inventoryLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        inventoryBox.getChildren().add(inventoryPanel);
        vBox.getChildren().addAll(witcherLabel, witcherPanel, inventoryLabel, inventoryBox);

        inventory.registerObserver(this);
        witcher.registerObserver(this);

        witcher.notifyObservers();
    }

    public VBox getPlayerPanel() {
        return vBox;
    }

    @Override
    public void update() {
        witcherPanel.getChildren().clear();
        inventoryBox.getChildren().clear();
        vBox.getChildren().clear();

        loadWitcherStatus();
        loadImagesOfItems();

        inventoryBox.getChildren().add(inventoryPanel);
        vBox.getChildren().addAll(witcherLabel, witcherPanel, inventoryLabel, inventoryBox);
        vBox.setPrefSize(WindowsDisplay.BOXES_WIDTH, WindowsDisplay.BOXES_HEIGHT);
    }

    //////////////////////////////////////////////////////// Private

    private void loadWitcherStatus() {
        Label witcherDesc = new Label(witcher.getDescription());
        witcherDesc.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        VBox moneyVBox = getMoneyVBox();
        VBox elixirsVBox = getElixirsVBox();

        witcherPanel.getChildren().addAll(witcherDesc, moneyVBox, elixirsVBox);

        if (inventory.isSomethingUpgraded()) {
            Label equipmentTextLabel = new Label(Inventory.EQUIPMENT_NAME);

            equipmentTextLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));
            HBox equipmentHBox = getUpgradedEquipmentHBox();

            VBox equipmentVBox = new VBox();
            equipmentVBox.getChildren().addAll(equipmentTextLabel, equipmentHBox);

            witcherPanel.getChildren().addAll(equipmentVBox);
        }
    }

    private VBox getElixirsVBox() {
        InputStream inputStream;
        Image image;

        inputStream = PlayerPanel.class.getResourceAsStream(POTION_PNG_ROOT_DIRECTORY);
        image = Start.getImage(inputStream, POTION, WindowsDisplay.WITCHER_STUFF_DISPLAY, WindowsDisplay.WITCHER_STUFF_DISPLAY);

        ImageView elixirsView = new ImageView(image);
        Label elixirsText = new Label(inventory.getTextedCurrentNumberOfPotions());

        VBox elixirsVBox = new VBox();
        elixirsVBox.getChildren().addAll(elixirsView, elixirsText);
        elixirsVBox.setAlignment(Pos.CENTER_RIGHT);

        return elixirsVBox;
    }

    private VBox getMoneyVBox() {
        InputStream inputStream = PlayerPanel.class.getResourceAsStream(MONEY_PURSE_ROOT_DIRECTORY);
        Image image = Start.getImage(inputStream, MONEY_PURSE, WindowsDisplay.WITCHER_STUFF_DISPLAY, WindowsDisplay.WITCHER_STUFF_DISPLAY);

        ImageView moneyView = new ImageView(image);
        Label moneyText = new Label(inventory.getTextedMoney());

        VBox moneyVBox = new VBox();
        moneyVBox.getChildren().addAll(moneyView, moneyText);
        moneyVBox.setAlignment(Pos.CENTER_LEFT);

        return moneyVBox;
    }

    private HBox getUpgradedEquipmentHBox() {
        VBox swordVBox = new VBox();
        VBox armorVBox = new VBox();
        VBox potionSacVBox = new VBox();

        HBox equipmentHBox = new HBox();
        equipmentHBox.setPrefWidth(WindowsDisplay.BOXES_WIDTH);

        Label equipmentTextLabel = new Label(Inventory.EQUIPMENT_NAME);
        equipmentTextLabel.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        Label equipmentLabel;
        InputStream inputStream;
        Image image;

        if (inventory.isUpradgedDamage()) {
            inputStream = PlayerPanel.class.getResourceAsStream(SWORD_ROOT_DIRECTORY);
            image = Start.getImage(inputStream, Inventory.SWORD, WindowsDisplay.EQUIPMENT_DISPLAY, WindowsDisplay.EQUIPMENT_DISPLAY);

            ImageView imageView = new ImageView(image);
            equipmentLabel = new Label(Inventory.SWORD);

            swordVBox.getChildren().addAll(imageView, equipmentLabel);
            swordVBox.setStyle("-fx-padding: 10px");
        }

        if (inventory.isUpradgedArmor()) {
            inputStream = PlayerPanel.class.getResourceAsStream(ARMOR_ROOT_DIRECTORY);
            image = Start.getImage(inputStream, Inventory.ARMOR, WindowsDisplay.EQUIPMENT_DISPLAY, WindowsDisplay.EQUIPMENT_DISPLAY);

            ImageView imageView = new ImageView(image);
            equipmentLabel = new Label(Inventory.ARMOR);

            armorVBox.getChildren().addAll(imageView, equipmentLabel);
            armorVBox.setStyle("-fx-padding: 10px");
        }

        if (inventory.isUpradgedPotionSizeInventory()) {
            inputStream = PlayerPanel.class.getResourceAsStream(POTION_SAC_ROOT_DIRECTORY);
            image = Start.getImage(inputStream, Inventory.POTION_SAC, WindowsDisplay.EQUIPMENT_DISPLAY, WindowsDisplay.EQUIPMENT_DISPLAY);

            ImageView imageView = new ImageView(image);
            equipmentLabel = new Label(Inventory.POTION_SAC);

            potionSacVBox.getChildren().addAll(imageView, equipmentLabel);
            potionSacVBox.setStyle("-fx-padding: 10px");
        }

        equipmentHBox.getChildren().addAll(swordVBox, armorVBox, potionSacVBox);

        return equipmentHBox;
    }

    private void loadImagesOfItems() {
        Collection<Item> inventoryItems = inventory.getItemsFromInventory();
        VBox itemVbox = new VBox();
        itemVbox.setPrefSize(WindowsDisplay.INVENTORY_WIDTH, WindowsDisplay.INVENTORY_HEIGHT);

        for (Item item : inventoryItems) {
            String imageNamePng = item.getName() + ".png";
            String imageRoot = "/resources/" + imageNamePng;

            InputStream inputStream = PlayerPanel.class.getResourceAsStream(imageRoot);
            Image image = Start.getImage(inputStream, imageNamePng, WindowsDisplay.ITEMS_IN_INVENTORY_DISPLAY, WindowsDisplay.ITEMS_IN_INVENTORY_DISPLAY);

            Label itemDesc = new Label(item.getFullDescription());
            ImageView imageView = new ImageView(image);

            itemVbox.getChildren().addAll(imageView, itemDesc);
        }

        inventoryPanel.setContent(itemVbox);
    }

}