package dk.witcherhra.gui;

import dk.witcherhra.game.*;
import dk.witcherhra.game.action.*;
import dk.witcherhra.start.Start;
import dk.witcherhra.start.WindowsDisplay;
import dk.witcherhra.util.Observer;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.io.InputStream;

public class PositionPanel implements Observer {

    final Game game;
    final GameWorld gameWorld;

    private final static String MOST_USED_ACTIONS = "Nejčastější akční metody:";

    private final AnchorPane anchorPane = new AnchorPane();
    private final VBox vBox = new VBox();

    private final static Label currentAreaLabel = new Label(GameLocation.CURRENT_AREA);
    private final static Label mostUsedActionsLabel = new Label(MOST_USED_ACTIONS);
    private final static Label listViewLabel = new Label(GameLocation.EXITS);

    private Area currentArea;
    private Area previousArea;
    private final Witcher witcher;
    private final Inventory inventory;
    private static String command;

    ListView<String> listViewExits = new ListView<>();
    ListView<String> listViewMostUsedActions = new ListView<>();
    ObservableList<String> mostUsedActions = FXCollections.observableArrayList();
    ObservableList<String> exits = FXCollections.observableArrayList();

    public PositionPanel(Game game) {
        this.game = game;
        this.gameWorld = game.getWorld();

        currentArea = gameWorld.getCurrentArea();
        previousArea = gameWorld.getPreviousArea();
        inventory = game.getInventory();
        witcher = game.getWitcher();

        updateCurrentAreaOptions();

        vBox.setPrefSize(WindowsDisplay.BOXES_WIDTH, WindowsDisplay.BOXES_HEIGHT);
        currentAreaLabel.setFont(Font.font("Arial", FontWeight.BOLD, 24));
        mostUsedActionsLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        listViewLabel.setFont(Font.font("Arial", FontWeight.BOLD, 20));

        game.registerObserver(this);
        gameWorld.registerObserver(this);
        currentArea.registerObserver(this);
        previousArea.registerObserver(this);
        witcher.registerObserver(this);
        inventory.registerObserver(this);
    }

    public VBox getPositionPanel() {
        return vBox;
    }

    @Override
    public void update() {
        vBox.getChildren().clear();
        exits.clear();
        mostUsedActions.clear();

        currentArea = gameWorld.getCurrentArea();
        previousArea = gameWorld.getPreviousArea();

        updateCurrentAreaOptions();
    }

    //////////////////////////////////////////////////////// Private

    private void updateCurrentAreaOptions() {
        if (game.getTasks().areTaskAssigned() && !game.isGameOver()) {
            render();
        }
    }

    private void render() {
        int currentAreaId = currentArea.getId();
        int previousAreaId = previousArea.getId();

        String currentAreaName = currentArea.getName();
        InputStream inputStream = PositionPanel.class.getResourceAsStream("/resources/" + currentAreaName + ".png");
        ImageView currentAreaView = new ImageView(Start.getImage(inputStream, currentAreaName, WindowsDisplay.CURRENT_AREA_WIDTH, WindowsDisplay.CURRENT_AREA_HEIGHT));
        anchorPane.getChildren().add(currentAreaView);

        exits.addAll(currentArea.getExitsForObserver());
        listViewExits.setItems(exits);

        prepareListViewExits();
        listViewExits.setPrefSize(WindowsDisplay.BOXES_HEIGHT, exits.size() * 25);

        Label currentAreaText = new Label(currentArea.getName());

        HBox currentAreaHBox = new HBox();
        currentAreaHBox.getChildren().addAll(currentAreaLabel, currentAreaText);
        currentAreaHBox.setAlignment(Pos.CENTER_LEFT);

        setMostUsedActions();
        listViewMostUsedActions.setPrefSize(WindowsDisplay.BOXES_HEIGHT, mostUsedActions.size() * 25);
        prepareListViewActions();

        vBox.getChildren().addAll(
                currentAreaHBox,
                currentAreaView
        );

        if (!currentArea.getName().equals(GameLocation.DRAGON_CAVE)) {
            if (previousAreaId != currentAreaId) {
                exits.add(GameLocation.FALL_BACK);
            }

            listViewExits.setPrefSize(WindowsDisplay.BOXES_HEIGHT, exits.size() * 25);
            vBox.getChildren().addAll(
                    listViewLabel,
                    listViewExits
            );
        }

        if (mostUsedActions.size() > 0) {
            vBox.getChildren().addAll(
                    mostUsedActionsLabel,
                    listViewMostUsedActions
            );
        }
    }

    private void prepareListViewExits() {
        listViewExits.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed()) {
                String selectedExit = listViewExits.getSelectionModel().getSelectedItem();
                TextArea console = Start.getConsole();
                String command;

                if (currentArea.hasExit(selectedExit)) {
                    command = new ActionMove(game).getName() + " " + selectedExit;

                    console.appendText("\n\n---\n" + Start.FILLED_COMMAND + command + "\n---\n" + "\n");
                    String answer = game.processAction(command);
                    console.appendText(answer);
                }

                if (selectedExit.equals(GameLocation.FALL_BACK)) {
                    command = new ActionFallBack(game).getName();

                    console.appendText("\n\n---\n" + Start.FILLED_COMMAND + command + "\n---\n" + "\n");
                    String answer = game.processAction(command);
                    console.appendText(answer);
                }
            }
        });
    }

    private void prepareListViewActions() {
        listViewMostUsedActions.setOnMouseClicked(mouseEvent -> {
            if (mouseEvent.getClickCount() == 2 && !mouseEvent.isConsumed()) {
                String command = listViewMostUsedActions.getSelectionModel().getSelectedItem();
                TextArea console = Start.getConsole();

                console.appendText("\n\n---\n" + Start.FILLED_COMMAND + command + "\n---\n" + "\n");
                String answer = game.processAction(command);
                console.appendText(answer);

                if (game.isGameOver()) {
                    TextField consoleTextField = Start.getConsoleTextField();
                    consoleTextField.setEditable(false);

                    console.appendText("\n" + game.getEpilogue());
                }
            }
        });
    }

    public void setMostUsedActions() {
        getMethodsForBlacksmith();
        getMethodsForPub();
        getMethodsForBrothel();
        getMethodsForAreaWithEnemy();
        getMethodsDrinkPotion();
        getMethodsEat();
        getMethodsPickAll();
        getMethodsDropAll();

        listViewMostUsedActions.setItems(mostUsedActions);
    }

    private void getMethodsForBlacksmith() {
        if (currentArea.getName().equals(GameLocation.BLACKSMITH)) {
            command = new ActionUpradgeItem(game).getName() + " ";
            if (!inventory.isUpradgedDamage()) {
                mostUsedActions.add(command + Inventory.SWORD);
            }
            if (!inventory.isUpradgedArmor()) {
                mostUsedActions.add(command + Inventory.ARMOR);
            }
            if (!inventory.isUpradgedPotionSizeInventory()) {
                mostUsedActions.add(command + Inventory.POTION_SAC);
            }
        }
    }

    private void getMethodsForPub() {
        if (currentArea.getName().equals(GameLocation.PUB)) {
            command = new ActionSleep(game).getName();

            mostUsedActions.add(command);
        }
    }

    private void getMethodsForBrothel() {
        if (currentArea.getName().equals(GameLocation.BROTHEL)) {
            command = new ActionHaveFun(game).getName();

            mostUsedActions.add(command);
        }
    }

    private void getMethodsForAreaWithEnemy() {
        if (currentArea.hasEnemy()) {
            command = new ActionAttack(game).getName();

            mostUsedActions.add(command);
        }
    }

    private void getMethodsDrinkPotion() {
        int maxHp = witcher.getMaxHP();
        int currentHp = witcher.getHpBar();
        int minimumHpLeft = maxHp - Inventory.HEALING_POTION_REGENERATE;

        if (currentHp <= minimumHpLeft && inventory.getNumberOfHealingPotions() > 0) {
            command = new ActionDrinkPotion(game).getName();

            mostUsedActions.add(command);
        }
    }

    private void getMethodsEat() {
        int maxHp = witcher.getMaxHP();
        int currentHp = witcher.getHpBar();
        int minimumHpLeft = maxHp - Item.STANDARD_HEALING_NUMBER;
        int itemHealingNumber;

        if (currentHp <= minimumHpLeft && !inventory.isInventoryEmpty()) {
            command = new ActionEat(game).getName();
            for (Item item : inventory.getItemsFromInventory()) {
                itemHealingNumber = item.getHealingNumber();
                minimumHpLeft = maxHp - itemHealingNumber;
                String itemName = item.getName();

                if (currentHp <= minimumHpLeft) {
                    mostUsedActions.add(command + " " + itemName);
                }
            }
        }
    }

    private void getMethodsPickAll() {
        if (currentArea.hasItems() && !inventory.isInventoryFull()) {
            command = new ActionPickAll(game).getName();

            mostUsedActions.add(command);
        }
    }

    private void getMethodsDropAll() {
        if (!inventory.isInventoryEmpty()) {
            command = new ActionDropAll(game).getName();

            mostUsedActions.add(command);
        }
    }

}
