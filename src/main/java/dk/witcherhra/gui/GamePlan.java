package dk.witcherhra.gui;

import dk.witcherhra.game.Area;
import dk.witcherhra.game.GameWorld;
import dk.witcherhra.start.Start;
import dk.witcherhra.start.WindowsDisplay;
import dk.witcherhra.util.Observer;
import javafx.geometry.Pos;
import javafx.scene.layout.AnchorPane;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.io.*;

public class GamePlan implements Observer {


    private static final String WORLD = "World";
    private static final String WITCHERS_HORSE = "Witchers_Horse";

    private static final String WORLD_PNG_ROOT_DIRECTORY = "/resources/" + WORLD + ".png";
    private static final String WITCHERS_HORSE_PNG_ROOT_DIRECTORY = "/resources/" + WITCHERS_HORSE + ".png";

    private final AnchorPane anchorPane = new AnchorPane();
    private final HBox hBox = new HBox();
    private static ImageView pointer;
    private final GameWorld gameWorld;

    public GamePlan(GameWorld gameWorld) {
        this.gameWorld = gameWorld;

        InputStream inputStream = GamePlan.class.getResourceAsStream(WORLD_PNG_ROOT_DIRECTORY);
        ImageView gameWorldView = new ImageView(Start.getImage(inputStream, WORLD, WindowsDisplay.GAME_PLAN_WIDTH_DISPLAY, WindowsDisplay.GAME_PLAN_HEIGHT_DISPLAY));

        inputStream = GamePlan.class.getResourceAsStream(WITCHERS_HORSE_PNG_ROOT_DIRECTORY);
        pointer = new ImageView(Start.getImage(inputStream, WITCHERS_HORSE, WindowsDisplay.HORSE_WIDTH_DISPLAY, WindowsDisplay.HORSE_HEIGHT_DISPLAY));

        Area currentArea = gameWorld.getCurrentArea();
        updateCurrentPositionPointer(currentArea);

        gameWorld.registerObserver(this);

        anchorPane.getChildren().addAll(gameWorldView, pointer);

        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(anchorPane);
    }

    public HBox getHBox()
    {
        return hBox;
    }

    @Override
    public void update() {
        updateCurrentPositionPointer(gameWorld.getCurrentArea());
    }

    //////////////////////////////////////////////////////// Private

    private void updateCurrentPositionPointer(Area currentArea)
    {
        AnchorPane.setTopAnchor(pointer, currentArea.getPositionTop());
        AnchorPane.setLeftAnchor(pointer, currentArea.getPositionLeft());
    }

}
