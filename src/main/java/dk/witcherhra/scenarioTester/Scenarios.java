package dk.witcherhra.scenarioTester;

import java.util.*;
import dk.witcherhra.game.*;

/**
 * Třída obsahující statickou množinu s testovacími scénáři pro tuto hru.
 *
 * @author Jan Říha
 * @author Daniel Kubánek
 * @version LS-2021, 2021-05-10
 */
public class Scenarios
{
    /**
     * Soukromý konstruktor. Zajistí, že nebude možné vytvářet instance této třídy.
     */
    private Scenarios()
    {
    }

    /** Množina testovacích scénářů. */
    public static final Set<Scenario> SCENARIOS = new HashSet<Scenario>() {{
            add((new Scenario("PRUCHOD_VESNICI")
            .addStep(new Step()
                .setAction("test")
                .setMessage("Pokud chcete hru opravdu začít, použijte příkaz 'Jdi Hostinec'"))
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.PUB)
                .setMessage(GameConversation.FIRST_PUB_VISIT)
                .setArea(GameLocation.PUB)
                .setExits(GameLocation.BROTHEL, GameLocation.SIGNPOST, GameLocation.BLACKSMITH))
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.BROTHEL)
                .setMessage(GameLocation.BROTHEL_DESC)
                .setArea(GameLocation.BROTHEL)
                .setExits(GameLocation.PUB, GameLocation.SIGNPOST, GameLocation.BLACKSMITH))
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.BLACKSMITH)
                .setMessage(GameLocation.BLACKSMITH_DESCRIPTION)
                .setArea(GameLocation.BLACKSMITH)
                .setExits(GameLocation.PUB, GameLocation.SIGNPOST, GameLocation.BROTHEL))
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.PUB)
                .setMessage(GameLocation.PUB_DESC)
                .setArea(GameLocation.PUB)
                .setExits(GameLocation.BROTHEL, GameLocation.SIGNPOST, GameLocation.BLACKSMITH))
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.SIGNPOST)
                .setMessage(GameLocation.SIGNPOST_DESC)
                .setArea(GameLocation.SIGNPOST)
                .setExits(GameLocation.WAY, GameLocation.CORNFIELD, GameLocation.OPEN_SPACE_FIELD, GameLocation.VILLAGE))
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.VILLAGE)
                .setMessage(GameLocation.VILLAGE_DESCRIPTION)
                .setArea(GameLocation.VILLAGE)
                .setExits(GameLocation.PUB, GameLocation.SIGNPOST, GameLocation.BROTHEL, GameLocation.BLACKSMITH))));
                    add((new Scenario("PRIKAZ_KONEC")
            .addStep(new Step()
                .setAction("Jdi " + GameLocation.PUB)
                .setMessage(GameConversation.FIRST_PUB_VISIT)
                .setArea(GameLocation.PUB)
                .setExits(GameLocation.BROTHEL, GameLocation.SIGNPOST, GameLocation.BLACKSMITH))
            .addStep(new Step()
                .setAction("Konec test")
                .setMessage("Pokud chcete ukončit hru, pouzijte pouze příkaz 'Konec'"))
            .addStep(new Step()
                .setAction("Konec")
                .setMessage("Hra byla ukončena příkazem 'Konec'."))));
    }};
}
