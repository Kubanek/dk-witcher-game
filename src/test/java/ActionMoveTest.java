import dk.witcherhra.game.*;
import dk.witcherhra.game.action.ActionMove;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testovací třída pro komplexní otestování třídy {@link ActionMove} a
 * dalších metod spadající pod IAction.
 *
 * @author Jan Říha
 * @author Daniel Kub8nek
 * @version LS-2021, 2021-03-13
 */
public class ActionMoveTest
{
    private Game game;
    private Enemy enemyOne;

    @BeforeEach
    public void setUp()
    {
        game = new Game();
        enemyOne = Encounters.getRandomEnemy();

        Area village = new Area(GameLocation.VILLAGE, GameLocation.VILLAGE_DESCRIPTION, 1.1, 1.1);
        Area brothel = new Area(GameLocation.BROTHEL, GameLocation.BROTHEL_DESC, 1.1, 1.1);
        Area pub = new Area(GameLocation.PUB, GameLocation.PUB_DESC, 1.1, 1.1);
        Area blacksmith = new Area(GameLocation.BLACKSMITH, GameLocation.BLACKSMITH_DESCRIPTION, 1.1, 1.1);
        Area signpost = new Area(GameLocation.SIGNPOST, GameLocation.SIGNPOST_DESC, 1.1, 1.1);

        Area wayOne = new Area(GameLocation.WAY, GameLocation.WAY_START_DESCRIPTION, 1.1, 1.1);
        Area wayTwo = new Area(GameLocation.WAY, GameLocation.WAY_MIDDLE_DESCRIPTION, 1.1, 1.1);
        Area wayThree = new Area(GameLocation.WAY, GameLocation.WAY_MIDDLE_DESCRIPTION, 1.1, 1.1);
        Area wayFour = new Area(GameLocation.WAY, GameLocation.WAY_END_DESCRIPTION, 1.1, 1.1);

        Area castle = new Area(GameLocation.CASTLE, GameLocation.CASTLE_DESC, 1.1, 1.1);

        village.addExit(brothel);
        village.addExit(pub);
        village.addExit(blacksmith);
        village.addExit(signpost);

        brothel.addExit(pub);
        brothel.addExit(blacksmith);
        brothel.addExit(signpost);

        pub.addExit(brothel);
        pub.addExit(blacksmith);
        pub.addExit(signpost);

        blacksmith.addExit(pub);
        blacksmith.addExit(brothel);
        blacksmith.addExit(signpost);

        Area dragonCave = new Area(GameLocation.DRAGON_CAVE, GameLocation.DRAGON_CAVE_DESC, 1.1, 1.1);

        game.getWorld().setCurrentArea(pub);

        signpost.addExit(village);
        signpost.addExit(wayOne, GameLocation.DIRECTION_EAST);

        wayOne.addExit(signpost, GameLocation.DIRECTION_WEST);
        wayOne.addExit(wayTwo, GameLocation.DIRECTION_EAST);
        wayTwo.addExit(wayThree, GameLocation.DIRECTION_NORTH);
        wayThree.addExit(wayFour, GameLocation.DIRECTION_EAST);
        wayFour.addExit(castle);
        castle.addExit(dragonCave);

        game.getTasks().setTaskAssigned(true);
    }

    @Test
    public void testMovement()
    {
        GameWorld world = game.getWorld();

        assertEquals("K přesunu je potřeba říct, kam se má zaklínač přemístit.", game.processAction("Jdi"));
        assertEquals("Jste bohužel jenom zaklínač, neumíte jít do více lokací současně najednou.", game.processAction("Jdi x y z"));
        assertEquals(GameLocation.PUB, world.getCurrentArea().getName());
        assertEquals("Do lokace 'xxx' se odsud jít nedá.", game.processAction("Jdi xxx"));
        assertTrue(game.processAction("Jdi " + GameLocation.BROTHEL).contains(GameLocation.BROTHEL_DESC));
        assertTrue(game.processAction("Jdi " + GameLocation.BLACKSMITH).contains(GameLocation.BLACKSMITH_DESCRIPTION));
        assertTrue(game.processAction("Jdi " + GameLocation.SIGNPOST).contains(GameLocation.SIGNPOST_DESC));
        assertTrue(game.processAction("Jdi " + GameLocation.DIRECTION_EAST).contains(GameLocation.WAY_START_DESCRIPTION));

        world.getCurrentArea().setEnemy(enemyOne);
        assertEquals(GameLocation.FAILED_CURRENT_LOCATION_CHANGE, game.processAction("Jdi Vychod"));
        assertEquals("Pokud se chcete přesunout do předchozí lokace, není potřeba lokaci vypisovat", game.processAction("Vrat_se xxx"));
        assertEquals(world.getPreviousArea().getFullDescription(), game.processAction("Vrat_se"));
        
        assertEquals(world.getCurrentArea().getFullDescription(), game.processAction("Rozhledni_se"));
        assertEquals("Jako zaklínači Ti stačí se porozhlednout kolem sebe, nepotřebeš se dívat na nic konkrétního.", game.processAction("Rozhledni_se xxx"));
        
        game.processAction("Jdi Vychod");
        world.getCurrentArea().setEnemy(null);
        assertTrue(game.processAction("Jdi Vychod").contains(GameLocation.WAY_MIDDLE_DESCRIPTION));
    }

    @Test
    public void testWinningScenarioAndSomeActions()
    {
        GameWorld world = game.getWorld();
        Tasks tasks = game.getTasks();

        game.processAction("Jdi " + GameLocation.SIGNPOST);
        game.processAction("Jdi " + GameLocation.DIRECTION_EAST);
        game.processAction("Jdi " + GameLocation.DIRECTION_EAST);
        game.processAction("Jdi " + GameLocation.DIRECTION_NORTH);
        assertTrue(game.processAction("Jdi Vychod").contains(GameLocation.WAY_END_DESCRIPTION));

        assertTrue(world.getCurrentArea().getName().equals(GameLocation.WAY));
        assertEquals(GameConversation.CASTLE_VISIT_BEFORE_TAKS_FINISH + world.getCurrentArea().getFullDescription(), game.processAction("Jdi Hrad"));
        tasks.setTasksFinished(true);
        assertTrue(world.getCurrentArea().getName().equals(GameLocation.WAY));
        assertEquals(GameConversation.FIRST_CASTLE_VISIT, game.processAction("Jdi " + GameLocation.CASTLE));

        assertEquals(GameLocation.NOBODY_TO_FIGHT, game.processAction("Bojuj"));
        assertEquals("Jste zaklínač, nepotřebujete specificky popisovat na koho útočíte, vždycky to jako zaklínač víte.", game.processAction("Bojuj xxx"));

        game.processAction("Jdi " + GameLocation.DRAGON_CAVE);

        assertTrue(world.getCurrentArea().getDescription().equals(GameLocation.DRAGON_CAVE_DESC));
        assertTrue(game.isGameOver());
    }

    @Test
    public void testTaskOneComplete()
    {
        game.processAction("Jdi " + GameLocation.SIGNPOST);
        game.getTasks().setTaskOneComplete(true);
        assertEquals(GameConversation.TASK_ONE_FINISHED + "\n" + "\nU sebe v Inventáři máš 120 zlotých.", game.processAction("Jdi " + GameLocation.VILLAGE));
    }
}
