import dk.witcherhra.game.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testovací třída pro komplexní otestování třídy {@link Area}.
 * Ve třídě se taky testují některé metody z IAction.
 *
 * @author Jan Říha
 * @version LS-2021, 2021-05-10
 */
public class AreaTest
{
    private Game game;

    @BeforeEach
    public void setUp()
    {
        game = new Game();

        Area village = new Area(GameLocation.VILLAGE, GameLocation.VILLAGE_DESCRIPTION, 1.1, 1.1);
        Area brothel = new Area(GameLocation.BROTHEL, GameLocation.BROTHEL_DESC, 1.1, 1.1);
        Area pub = new Area(GameLocation.PUB, GameLocation.PUB_DESC, 1.1, 1.1);
        Area blacksmith = new Area(GameLocation.BLACKSMITH, GameLocation.BLACKSMITH_DESCRIPTION, 1.1, 1.1);
        Area signpost = new Area(GameLocation.SIGNPOST, GameLocation.SIGNPOST_DESC, 1.1, 1.1);

        village.addExit(brothel);
        village.addExit(pub);
        village.addExit(blacksmith);
        village.addExit(signpost);

        brothel.addExit(pub);
        brothel.addExit(blacksmith);
        brothel.addExit(signpost);

        pub.addExit(brothel);
        pub.addExit(blacksmith);
        pub.addExit(signpost);

        blacksmith.addExit(pub);
        blacksmith.addExit(brothel);
        blacksmith.addExit(signpost);

        signpost.addExit(village);
        game.getWorld().setCurrentArea(pub);

        Item beer = new Item(Item.BEER, Item.BEER_DESC, true, Item.STANDARD_HEALING_NUMBER);
        Item vodka = new Item(Item.VODKA, Item.VODKA_DESC, true, Item.VODKA_HEALING_NUMBER);
        Item chicken = new Item(Item.CHICKEN, Item.CHICKEN_DESC, true, Item.CHICKEN_HEALING_NUMBER);
        Item chair = new Item(Item.CHAIR, Item.CHAIR_DESC);

        pub.addItem(beer);
        pub.addItem(vodka);
        pub.addItem(chicken);
        pub.addItem(chair);  

        game.getTasks().setTaskAssigned(true);
    }

    @Test
    public void testExitsAndItemActions()
    {
        GameWorld world = game.getWorld();

        assertEquals(GameLocation.PUB, world.getCurrentArea().getName());
        assertEquals(3, world.getCurrentArea().getExits().size());

        game.processAction("Jdi " + GameLocation.SIGNPOST);
        assertEquals(1, world.getCurrentArea().getExits().size());

        game.processAction("Jdi " + GameLocation.VILLAGE);
        game.processAction("Jdi " + GameLocation.PUB);

        assertEquals("Předmět 'xxx' tady není.", game.processAction("Seber xxx"));
        assertEquals(Inventory.INVENTORY_IS_EMPTY, game.processAction("Vyhod Kure"));
        assertEquals("Sebral jsi předmět 'Pivo' a uložil jsi ho do inventáře.", game.processAction("Seber Pivo"));
        assertEquals("Jste zaklínač, předmět 'Zidle' fakt nepotřebuješ.", game.processAction("Seber Zidle"));
        assertEquals("Pro sebrání všech věcí z dané lokace není potřeba vypisovat jednotlivé předměty", game.processAction("Seber_vse xxx"));
        assertTrue(world.getCurrentArea().containsItem("Vodka"));
        assertFalse(world.getCurrentArea().containsItem("xxx"));
        assertEquals("Sebral(a) jste tyto předměty: Kure, Vodka,", game.processAction("Seber_vse"));
        assertEquals("Zahodil jsi předmět 'Kure'. Tenhle předmět se teď nachází v této lokaci.", game.processAction("Vyhod Kure"));
        assertEquals("V inventáři žádný předmět 'xxx' není.", game.processAction("Vyhod xxx"));

        assertEquals("V inventáři žádný předmět 'xxx' není.", game.processAction("Snez xxx"));
        assertEquals("Cítíš se plný sil, nepotřebuješ nic jíst.", game.processAction("Snez Pivo"));
        game.getWitcher().setHpBar(50);
        assertEquals("Snědl jste předmět 'Pivo' Počet aktuálních HP se změnil na 60.",  game.processAction("Snez Pivo"));
        assertEquals("Vypili jste léčebný lektvar. Počet aktuálních HP se změnil na 100\n" + "Počet dostupných lěčebných lektvarů: 0 z 1", game.processAction("Vypij_elixir"));
        assertEquals("V inventáři nemátě žádný léčebný elixít na vypití.", game.processAction("Vypij_elixir"));
        game.getWitcher().setHpBar(50);
        assertEquals("V inventáři nemátě žádný léčebný elixít na vypití.", game.processAction("Vypij_elixir"));
        assertEquals("Jste zaklínač, který má u sebe pouze elixíry doplnění života, nepotřebujete tedy popisovat jaky elixír chcete vypít", game.processAction("Vypij_elixir xxx"));
    }

    @Test
    public void testSleepAndHaveFun()
    {
        game.processAction("Jdi " + GameLocation.BROTHEL);
        game.getWitcher().setHpBar(50);
        assertEquals(GameLocation.BROTHEL_AFTER_HAVING_FUN,  game.processAction("Odpocin_si"));
        assertEquals("Navštívení nevěstince stojí 20 zlotých.\n" + "Ve tvém inventáři se nachází jenom " + game.getInventory().getMoney() + " zlotých",  game.processAction("Odpocin_si"));
        game.getInventory().setMoney(2000);
        game.getWitcher().setHpBar(100);
        assertEquals("Cítíš se plný sil, nepotřebuješ si odpočinout (užít).", game.processAction("Odpocin_si"));
        game.processAction("Jdi " + GameLocation.PUB);
        assertEquals("Cítíš se plný sil, nepotřebuješ spát", game.processAction("Vyspi_se"));
        game.getWitcher().setHpBar(10);
        assertEquals(GameLocation.PUB_AFTER_SLEEPING, game.processAction("Vyspi_se"));
        assertEquals(100, game.getWitcher().getHpBar());
    }

    @Test
    public void testUpradgedItem()
    {
        game.processAction("Jdi " + GameLocation.BROTHEL);
        assertEquals("Své vybavení si můžeš vylepšit jen u kováře.",  game.processAction("Vylepsi"));
        game.processAction("Jdi " + GameLocation.BLACKSMITH);
        assertEquals("Předmět 'Mec' stojí 200 zlotých.\n" + "Ve tvém inventáři se nachází jenom 20 zlotých",  game.processAction("Vylepsi " + Inventory.SWORD));
        game.getInventory().setMoney(2000);
        assertEquals("Vybavení '" + Inventory.SWORD + "' bylo vylepšeno",  game.processAction("Vylepsi " + Inventory.SWORD));
        assertEquals(GameLocation.THIS_ITEM_CANNOT_BE_UPRADGED_AGAIN, game.processAction("Vylepsi " + Inventory.SWORD));
    }
}
